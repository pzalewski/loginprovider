﻿using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Collections.Specialized;
using System.Web.Profile;
using System.Linq;


namespace HSOLoginProvider
{
    public class HSOProfileProvider : ProfileProvider
    {
        private string name;
        public override string Name
        {
            get { return name; }
        }

        private string connectionString;
        public string ConnectionString
        {
            get { return connectionString; }
        }

        

        private string getProcedure;
        public string GetUserProcedure
        {
            get { return getProcedure; }
        }

        // System.Configuration.Provider.ProviderBase.Initialize Method
        public override void Initialize(string name, NameValueCollection config)
        {
            // Initialize values from web.config.
            this.name = name;

            ConnectionStringSettings connectionStringSettings = ConfigurationManager.
                ConnectionStrings[config["connectionStringName"]];
            if (connectionStringSettings == null ||
                connectionStringSettings.ConnectionString.Trim() == "")
            {
                throw new Exception("You must supply a connection string.");
            }
            else
            {
                connectionString = connectionStringSettings.ConnectionString;
            }

            getProcedure = config["getUserProcedure"];
            if (getProcedure.Trim() == "")
            {
                throw new Exception("You must specify a stored procedure to use for retrieving individual user records.");
            }
        }

        public override int DeleteProfiles(string[] usernames)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override int DeleteProfiles(ProfileInfoCollection profiles)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override int DeleteInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override ProfileInfoCollection FindInactiveProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch, DateTime userInactiveSinceDate, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override ProfileInfoCollection FindProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override ProfileInfoCollection GetAllInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override ProfileInfoCollection GetAllProfiles(ProfileAuthenticationOption authenticationOption, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override int GetNumberOfInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public override string ApplicationName
        {
            get
            {
                throw new Exception("The method or operation is not implemented.");
            }
            set
            {
                throw new Exception("The method or operation is not implemented.");
            }
        }

        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection properties)
        {
            // If you want to mimic the behavior of the SqlProfileProvider,
            // you should also update the database with the last activity time
            // whenever this method is called.

            // This collection will store the retrieved values.
            SettingsPropertyValueCollection values = new SettingsPropertyValueCollection();

            // Prepare the command.
            // The only non-configurable assumption in this code is
            // that the stored procedure accepts a parameter named
            // @UserName. You could add additional configuration attributes
            // to make this detail configurable.

            string username = context["UserName"].ToString();
            string[] userAndBranch = username.Split('#');

            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(getProcedure, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@IdUser", userAndBranch[0]));
            if (userAndBranch.Count() > 1)
                cmd.Parameters.AddWithValue("IdBranch", userAndBranch[1]);
            try
            {
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.Default);

                // Get the first row.
                reader.Read();

                // Look for every requested value.
                foreach (SettingsProperty property in properties)
                {                    
                    SettingsPropertyValue value = new SettingsPropertyValue(property);

                    // Non-matching users are allowed
                    // (properties are kept, but no values are added).
                    if (reader.HasRows)
                    {
                        value.PropertyValue = reader[property.Name];
                    }
                    values.Add(value);
                }
                reader.Close();
            }
            finally
            {
                con.Close();
            }

            return values;
        }

        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection values)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public Dictionary<int, string> GetManagers(int idUser)
        {
            Dictionary<int, string> Managers = new Dictionary<int, string>();
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("HSOLoginProviderGetUserManagers", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("IdUser", idUser);
            con.Open();
            using (SqlDataReader sqlDr = cmd.ExecuteReader())
            {
                while (sqlDr.Read())
                {
                    Managers.Add(Convert.ToInt32(sqlDr["Id"]),
                    sqlDr["Nazwisko"].ToString() + " " + sqlDr["Imie"].ToString());
                }
            }
            con.Close();
            return Managers;
        }

        public bool IsManager(int idUser)
        {
            bool isManager = false;
            Dictionary<int, string> Managers = new Dictionary<int, string>();
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("HSOLoginProviderIsManager", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("IdUser", idUser);
            con.Open();
            using (SqlDataReader sqlDr = cmd.ExecuteReader())
            {
                while (sqlDr.Read())
                {
                    if (Convert.ToInt32(sqlDr["CzyKierownik"]) > 0)
                    {
                        isManager = true;
                    }
                }
            }
            con.Close();
            return isManager;
        }

        //public List<int> GetDependentNoSKOK(int idUser)
        //{
        //    List<int> DependentNoSKOK = new List<int>();
        //    SqlConnection con = new SqlConnection(connectionString);
        //    SqlCommand cmd = new SqlCommand("HSOLoginProviderGetDependetNoSKOK", con);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.Parameters.AddWithValue("IdUser", idUser);
        //    con.Open();
        //    using (SqlDataReader sqlDr = cmd.ExecuteReader())
        //    {
        //        while (sqlDr.Read())
        //        {
        //            if(sqlDr["NoSKOK"] != DBNull.Value)
        //                DependentNoSKOK.Add(Convert.ToInt32(sqlDr["NoSKOK"]));
        //        }
        //    }
        //    con.Close();
        //    return DependentNoSKOK;
        //}

    }
}
