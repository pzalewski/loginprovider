﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace HSOLoginProvider.Membership
{
    class HSOBranchStore
    {
        private static HSOBranchStore engine = new HSOBranchStore();

        private HSOBranchStore()
        {
            
        }

        public static HSOBranchStore Instance
        {
            get { return engine; }
        }

        public List<HSOBranch> GetBranchForUser(int IdUser, string  connStr)
        { 
            List<HSOBranch> branches = new List<HSOBranch>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connStr;
                conn.Open();
                SqlCommand cmd = new SqlCommand(@"HSOLoginProviderGetUserBranches", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdUser", IdUser);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    HSOBranch b = new HSOBranch();
                    if(r["IdBranch"] != DBNull.Value)
                        b.IdBranch = Convert.ToInt32(r["IdBranch"]);
                    b.Name = r["Name"].ToString();
                    if (r["NoBranch"] != DBNull.Value)
                        b.NoBranch = Convert.ToInt32(r["NoBranch"]);
                    if (r["NoSKOK"] != DBNull.Value)
                        b.NoSKOK = Convert.ToInt32(r["NoSKOK"]);
                    branches.Add(b);
                }
            }
            return branches;
        }
    }
}
