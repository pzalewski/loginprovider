﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSOLoginProvider.Membership
{
    public struct HSOBranch
    {
        public int IdBranch { get; set; }
        public string Name { get; set; }
        public int NoBranch { get; set; }
        public int NoSKOK { get; set; }
    }
}
