﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.DirectoryServices;
using HSOLoginProvider.Membership;
using System.Data.SqlClient;
using System.Data;

namespace HSOLoginProvider
{
    internal class HSOUserStore
    {

        private static HSOUserStore engine = new HSOUserStore();

        //string _cnUser;
        //private HSOUser _usr;

        private HSOUserStore()
        {
            //ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings["cnUser"];
            //_cnUser = settings.ConnectionString;
        }

        public static HSOUserStore Instance
        {
            get { return engine; }
        }

        public string GetFilterString(string kategoria, string filtr)
        {
            string resultat = String.Format("(&(objectCategory={0})(sAMAccountName={1}))", kategoria, filtr);
            return "(|" + resultat + ")";
        }

        public int ValidateUserInDomain(string usrName, int FirmaId, string password, string appName, string connectionString)
        {
            int _userId = 0;
            Dictionary<int, HSODomain> domainDict = HSODomainStore.Instance.GetDomainDict(appName, connectionString);
            if(domainDict.ContainsKey(FirmaId))
            {
                HSODomain d = domainDict[FirmaId];
                DirectoryEntry Base = new DirectoryEntry(d.DomainLdapPath, usrName.Trim(), password.Trim());
                DirectorySearcher ds = new DirectorySearcher();
                ds.SearchRoot = Base;
                ds.Filter = GetFilterString("user", usrName.Trim());	
                ds.PropertiesToLoad.Add("samaccountname");
                ds.PropertiesToLoad.Add("userAccountControl");
                ds.PropertiesToLoad.Add("pager");
                ds.PropertiesToLoad.Add("cn");		
                ds.ReferralChasing = ReferralChasingOption.None;
                SearchResult sr = null;
                try
                {
                    sr = ds.FindOne();

                    string s = sr.Path;
                    // pobierz z AD imie i nazwisko
                    try
                    {

                        ResultPropertyValueCollection cn = sr.Properties["cn"];
                        ResultPropertyValueCollection pager = sr.Properties["pager"];
                        ResultPropertyValueCollection disabledAccount = sr.Properties["userAccountControl"];
                        if (disabledAccount[0].ToString() == "514" || disabledAccount[0].ToString() == "66050")
                        { }
                        else
                        {
                            string[] imienazwisko = cn[0].ToString().Split(' ');
                            _userId = int.Parse(pager[0].ToString());
                            

                            
                        }
                    }
                    //Staramy się tak nie robić, chyba że jest to konieczne
                    catch (Exception ex)
                    {
                        string txt = ex.Message;
                    }

                }
                //Wyjątek za pomocą którego wyświetlany jest komunikat o niepoprawnej nazwie uzytkownika lub hasle
                catch (Exception ex)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(ex.Message))
                        {

                        }
                    }
                    catch
                    {

                    }
               }
            }
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connectionString;
                conn.Open();
                SqlCommand cmd = new SqlCommand(@"SELECT  COUNT(*) FROM dbo.[User]
                                                                            WHERE   IdUser = @userId
                                                                                    AND IsActive = 1", conn);
                cmd.Parameters.Add("@userId", SqlDbType.Int).Value = _userId;
                int isActive = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                if (isActive != 1)
                {
                    throw new System.Exception("Nieprawidłowy użytkownik");
                }
            }
            return _userId;
        }
        


        

    }
}
