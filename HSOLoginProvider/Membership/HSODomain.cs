﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSOLoginProvider.Membership
{
    public struct HSODomain
    {
        public int DomainId { get; set; }
        public string DomainName { get; set; }
        public string DomainLdapPath { get; set; }
    }
}
