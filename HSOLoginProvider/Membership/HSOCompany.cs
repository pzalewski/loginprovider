﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSOLoginProvider.Membership
{
    public class HSOCompany
    {
        public int IdCompany { get; set; }
        public string Name { get; set; }
        public int NoSKOK { get; set; }
    }
}
