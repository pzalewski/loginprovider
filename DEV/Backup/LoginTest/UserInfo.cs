﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LoginTest
{
    public class UserInfo
    {
        public int Id { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Email { get; set; }
        public int StanowiskoId { get; set; }
        public string Stanowisko { get; set; }
        public string Dzial { get; set; }
        public int DzialId { get; set; }
        public int OddzialId { get; set; }
        public string Oddzial { get; set; }
        public int NrOddzialu { get; set; }
        public int NrSKOK { get; set; }
        public int RegionId { get; set; }
        public string Region { get; set; }
        public int KierownikId { get; set; }
        public string KierownikImie { get; set; }
        public string KierownikNazwisko { get; set; }
        public Dictionary<int, string> Kierownicy { get; set; }
        public bool CzyKierownik { get; set; }
        public int FirmaId { get; set; }
        public string Firma { get; set; }
        public string FirmaSuffix { get; set; }
        public string login { get; set; }
        public int ObslugiwanaFirmaId { get; set; }
        public string ObslugiwanaFirma { get; set; }
    }
}