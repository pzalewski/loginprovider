﻿using System;
using System.Web.Profile;
using System.Web.Security;

namespace LoginTest
{
    public class UserProfile : ProfileBase
    {
        public static UserProfile GetUserProfile(string username)
        {
            return Create(username) as UserProfile;
        }
        public static UserProfile GetUserProfile()
        {
            return Create(Membership.GetUser().UserName) as UserProfile;
        }

        [SettingsAllowAnonymous(false)]
        public int Id
        {
            get { return Convert.ToInt32(base["Id"]); }
        }

        [SettingsAllowAnonymous(false)]
        public string Imie
        {
            get { return base["Imie"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string Nazwisko
        {
            get { return base["Nazwisko"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public int IdStanowisko
        {
            get { return Convert.ToInt32(base["IdStanowisko"]); }
        }

        [SettingsAllowAnonymous(false)]
        public string Stanowisko
        {
            get { return base["Stanowisko"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public int IdDzial
        {
            get { return Convert.ToInt32(base["IdDzial"]); }
        }

        [SettingsAllowAnonymous(false)]
        public string Dzial
        {
            get { return base["Dzial"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public int IdOddzial
        {
            get { return Convert.ToInt32(base["IdOddzial"]); }
        }

        [SettingsAllowAnonymous(false)]
        public string Oddzial
        {
            get { return base["Oddzial"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public int NrOddzialu
        {
            get { return Convert.ToInt32(base["NrOddzialu"]); }
        }

        [SettingsAllowAnonymous(false)]
        public int NrSKOK
        {
            get { return Convert.ToInt32(base["NrSKOK"]); }
        }

        [SettingsAllowAnonymous(false)]
        public int IdRegion
        {
            get { return Convert.ToInt32(base["IdRegion"]); }
        }

        [SettingsAllowAnonymous(false)]
        public string Region
        {
            get { return base["Region"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public int IdKierownika
        {
            get { return Convert.ToInt32(base["IdKierownika"]); }
        }

        [SettingsAllowAnonymous(false)]
        public string NazwiskoKierownika
        {
            get { return base["NazwiskoKierownika"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string ImieKierownika
        {
            get { return base["ImieKierownika"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public int IdFirma
        {
            get { return Convert.ToInt32(base["IdFirma"]); }
        }

        [SettingsAllowAnonymous(false)]
        public string Firma
        {
            get { return base["Firma"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string FirmaAdres
        {
            get { return base["FirmaAdres"] as string; }
        }

        [SettingsAllowAnonymous(false)]
        public string Suffix
        {
            get { return base["Suffix"] as string; }
        }


    }

    public class BranchInfo
    {
        public int IdBranch;
        public string Name;
        public int NoBranch;
        public bool IsPk;
    }
}
