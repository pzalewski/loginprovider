﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Profile;
using LoginTest;


/// <summary>
/// Summary description for LoginHelper
/// </summary>
public class LoginEngine
{

    private static LoginEngine _instance = new LoginEngine();
    private HSOLoginProvider.HSOMembershipUser memb;
    private LoginEngine()
    {
        memb = (HSOLoginProvider.HSOMembershipUser)Membership.Providers["HSOMembershipProvider"];
    }

    public static LoginEngine Instance
    {
        get
        {
            return _instance;
        }
    }

    public int ValidateAndGetUserId(string username, string password)
    {
        if (Membership.ValidateUser(username, password))
        {
            int IdUser = memb.GetUserIdFromDatabase(username, password);
            return IdUser;
        }
        else
            return -1;
    }

    public List<HSOLoginProvider.Membership.HSOBranch> GetUserBranches(int idUser)
    {
        List<HSOLoginProvider.Membership.HSOBranch> branches = memb.GetBranchList(idUser);
        return branches;
    }

    public List<HSOLoginProvider.Membership.HSOCompany> GetUserCompanies(string idUser)
    {
        List<HSOLoginProvider.Membership.HSOCompany> companies = memb.GetCompaniesForUser(idUser);
        return companies;
    }

    public HttpCookie GetLogOnUserCookieById(string username)
    {
        MembershipUser usr = (MembershipUser)memb.GetUserById(username);
        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(usr.UserName, true, 20);
        string hash = FormsAuthentication.Encrypt(ticket);
        HttpCookie cookie = new HttpCookie(
            FormsAuthentication.FormsCookieName,
            hash);
        return cookie;
    }

    public UserInfo AuthorizeUser(string username)
    {

        List<HSOLoginProvider.Membership.HSOCompany> companies = LoginEngine.Instance.GetUserCompanies(username);
        HSOLoginProvider.HSOProfileProvider pr = (HSOLoginProvider.HSOProfileProvider)ProfileManager.Providers["HSOProfileProvider"];
        UserProfile profile = UserProfile.GetUserProfile(username);
        UserInfo u = new UserInfo();
        u.Id = profile.Id;
        u.Imie = profile.Imie;
        u.Nazwisko = profile.Nazwisko;
        u.StanowiskoId = profile.IdStanowisko;
        u.Stanowisko = profile.Stanowisko;
        u.DzialId = profile.IdDzial;
        u.Dzial = profile.Dzial;
        u.OddzialId = profile.IdOddzial;
        u.Oddzial = profile.Oddzial;
        u.NrOddzialu = profile.NrOddzialu;
        u.NrSKOK = profile.NrSKOK;
        u.RegionId = profile.IdRegion;
        u.Region = profile.Region;
        u.KierownikId = profile.IdKierownika;
        u.KierownikImie = profile.ImieKierownika;
        u.KierownikNazwisko = profile.NazwiskoKierownika;
        u.Kierownicy = pr.GetManagers(u.Id);
        u.CzyKierownik = pr.IsManager(u.Id);
        u.FirmaId = profile.IdFirma;
        u.Firma = profile.Firma;
        u.FirmaSuffix = profile.Suffix;
        if (companies.Count > 0)
        {
            u.ObslugiwanaFirmaId = companies.FirstOrDefault().IdCompany;
            u.ObslugiwanaFirma = companies.FirstOrDefault().Name;
        }
        else
        {
            u.ObslugiwanaFirmaId = profile.IdFirma;
            u.ObslugiwanaFirma = profile.Firma;
        }
        return u;
    }



}