﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace HSOLoginProvider.Membership
{
    class HSOCompanyStore
    {
        private static HSOCompanyStore engine = new HSOCompanyStore();

        private HSOCompanyStore()
        {
            
        }

        public static HSOCompanyStore Instance
        {
            get { return engine; }
        }

        public List<HSOCompany> GetCompaniesForUser(string idUser, string  connStr)
        {
            string[] _idUser = idUser.Split('#');

            List<HSOCompany> DependentCompanies = new List<HSOCompany>();

            if (_idUser.Count() == 1)
            {

                SqlConnection con = new SqlConnection(connStr);
                SqlCommand cmd = new SqlCommand("HSOLoginProviderGetDependetNoSKOK", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("IdUser", _idUser[0]);
                con.Open();
                using (SqlDataReader sqlDr = cmd.ExecuteReader())
                {
                    while (sqlDr.Read())
                    {
                        HSOCompany c = new HSOCompany();
                        if (sqlDr["NoSKOK"] != DBNull.Value)
                        {
                            c.IdCompany = Convert.ToInt32(sqlDr["IdCompany"]);
                            c.Name = sqlDr["Name"].ToString();
                            c.NoSKOK = Convert.ToInt32(sqlDr["NoSKOK"]);
                            DependentCompanies.Add(c);
                        }
                    }
                    sqlDr.NextResult();
                    while (sqlDr.Read())
                    {
                        HSOCompany c = new HSOCompany();
                        if (sqlDr["NoSKOK"] != DBNull.Value)
                        {
                            c.IdCompany = Convert.ToInt32(sqlDr["IdCompany"]);
                            c.Name = sqlDr["Name"].ToString();
                            c.NoSKOK = Convert.ToInt32(sqlDr["NoSKOK"]);
                            DependentCompanies.Add(c);
                        }
                    }
                }
                con.Close();
            }
            else
            {
                SqlConnection con = new SqlConnection(connStr);
                SqlCommand cmd = new SqlCommand("HSOLoginProviderGetDependetNoSKOKByBranch", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("IdBranch", _idUser[1]);
                con.Open();
                using (SqlDataReader sqlDr = cmd.ExecuteReader())
                {
                    while (sqlDr.Read())
                    {
                        HSOCompany c = new HSOCompany();
                        if (sqlDr["NoSKOK"] != DBNull.Value)
                        {
                            c.IdCompany = Convert.ToInt32(sqlDr["IdCompany"]);
                            c.Name = sqlDr["Name"].ToString();
                            c.NoSKOK = Convert.ToInt32(sqlDr["NoSKOK"]);
                            DependentCompanies.Add(c);
                        }
                    }
                }
                con.Close();
            }
            
            return DependentCompanies;
        }
    }
}
