﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HSOLoginProvider.Membership
{
    public class HSOProfile
    {
        public int IdUser { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String JobName { get; set; }
        public String nrTel { get; set; }
        public String Email { get; set; }
        public String Footer { get; set; }
        public List<String> Roles { get; set; }
    }
}
