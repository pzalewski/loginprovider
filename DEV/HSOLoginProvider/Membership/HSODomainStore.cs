﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace HSOLoginProvider.Membership
{
    internal class HSODomainStore
    {
        private static HSODomainStore engine = new HSODomainStore();

        //private HSODomain[] _HSODomains;
        private Dictionary<int, HSODomain> _DomainDict;

        private HSODomainStore()
        {
            
        }

        public static HSODomainStore Instance
        {
            get { return engine; }
        }

        public HSODomain[] CreateDomainList(string appName, string  connStr)
        { 
            List<HSODomain> domain = new List<HSODomain>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connStr;
                conn.Open();
                SqlCommand cmd = new SqlCommand(@"SELECT    f.firmaId,
                                                            f.Nazwa,
                                                            f.LDAP
                                                    FROM    dbo.DomenyAplikacji d
                                                            LEFT JOIN dbo.UprawnieniaAplikacje app ON d.AplikacjaId = app.IdAplikacji
                                                            LEFT JOIN dbo.Firma f ON d.FirmaId = f.FirmaId
                                                    WHERE   app.Nazwa = @nazwa
                                                            AND f.Aktywny = 1 ", conn);
                cmd.Parameters.Add("@nazwa", SqlDbType.VarChar).Value = appName;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    HSODomain d = new HSODomain();
                    d.DomainId = Convert.ToInt32(r["FirmaID"]);
                    d.DomainName = r["Nazwa"].ToString();
                    d.DomainLdapPath = r["LDAP"].ToString();
                    domain.Add(d);
                }

            }
            return domain.ToArray();
        }

        public Dictionary<int, HSODomain> GetDomainDict(string appName, string connStr)
        {
            List<HSODomain> domain = new List<HSODomain>();
            _DomainDict = new Dictionary<int, HSODomain>();
            using (SqlConnection conn = new SqlConnection())
            {
                conn.ConnectionString = connStr;
                conn.Open();
                SqlCommand cmd = new SqlCommand(@"SELECT    f.firmaId,
                                                            f.Nazwa,
                                                            f.LDAP
                                                    FROM    dbo.DomenyAplikacji d
                                                            LEFT JOIN dbo.UprawnieniaAplikacje app ON d.AplikacjaId = app.IdAplikacji
                                                            LEFT JOIN dbo.Firma f ON d.FirmaId = f.FirmaId
                                                    WHERE   app.Nazwa = @nazwa
                                                            AND f.Aktywny = 1 ", conn);
                cmd.Parameters.Add("@nazwa", SqlDbType.VarChar).Value = appName;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    HSODomain d = new HSODomain();
                    d.DomainId = Convert.ToInt32(r["FirmaID"]);
                    d.DomainName = r["Nazwa"].ToString();
                    d.DomainLdapPath = r["LDAP"].ToString();
                    domain.Add(d);
                    _DomainDict.Add(Convert.ToInt32(r["FirmaID"]), d);
                }

            }
            return _DomainDict;
        }

        //public HSODomain[] HSODomains
        //{
        //    get { return _HSODomains; }
        //}

        //public Dictionary<int, HSODomain> DomainDict
        //{
        //    get { return _DomainDict; }
        //}
    }
}
