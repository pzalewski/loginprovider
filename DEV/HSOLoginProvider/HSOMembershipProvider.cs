﻿using System;
using System.Web.Security;
using HSOLoginProvider.Membership;
using System.Configuration;
using System.Collections.Generic;

namespace HSOLoginProvider
{
    public sealed class HSOMembershipUser : MembershipProvider
    {

        private string connectionString;
        public string ConnectionString
        {
            get { return connectionString; }
        }

        private string _Name;
        private string _FileName;
        //private UserStore _CurrentStore = null;
        private string _ApplicationName;
        private bool _EnablePasswordReset;
        private bool _RequiresQuestionAndAnswer;
        private string _PasswordStrengthRegEx;
        private int _MaxInvalidPasswordAttempts;
        private int _MinRequiredNonAlphanumericChars;
        private int _MinRequiredPasswordLength;
        private MembershipPasswordFormat _PasswordFormat;

        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            if (string.IsNullOrEmpty(name))
            {
                name = "HSOMembershipProvider";
            }
            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "HSO Membership Provider");
            }

            ConnectionStringSettings connectionStringSettings = ConfigurationManager.
               ConnectionStrings[config["connectionStringName"]];
            if (connectionStringSettings == null ||
                connectionStringSettings.ConnectionString.Trim() == "")
            {
                throw new Exception("You must supply a connection string.");
            }
            else
            {
                connectionString = connectionStringSettings.ConnectionString;
            }

            // Initialize the base class
            base.Initialize(name, config);

            // Initialize default values
            _ApplicationName = "DefaultApp";
            _EnablePasswordReset = false;
            _PasswordStrengthRegEx = @"[\w| !§$%&/()=\-?\*]*";
            _MaxInvalidPasswordAttempts = 3;
            _MinRequiredNonAlphanumericChars = 1;
            _MinRequiredPasswordLength = 5;
            _RequiresQuestionAndAnswer = false;
            _PasswordFormat = MembershipPasswordFormat.Hashed;

            // Now go through the properties and initialize custom values
            foreach (string key in config.Keys)
            {
                switch (key.ToLower())
                {
                    case "name":
                        _Name = config[key];
                        break;
                    case "applicationname":
                        _ApplicationName = config[key];
                        break;
                    case "filename":
                        _FileName = config[key];
                        break;
                    case "enablepasswordreset":
                        _EnablePasswordReset = bool.Parse(config[key]);
                        break;
                    case "passwordstrengthregex":
                        _PasswordStrengthRegEx = config[key];
                        break;
                    case "maxinvalidpasswordattempts":
                        _MaxInvalidPasswordAttempts = int.Parse(config[key]);
                        break;
                    case "minrequirednonalphanumericchars":
                        _MinRequiredNonAlphanumericChars = int.Parse(config[key]);
                        break;
                    case "minrequiredpasswordlength":
                        _MinRequiredPasswordLength = int.Parse(config[key]);
                        break;
                    case "passwordformat":
                        _PasswordFormat = (MembershipPasswordFormat)Enum.Parse(
                                    typeof(MembershipPasswordFormat), config[key]);
                        break;
                    case "requiresquestionandanswer":
                        _RequiresQuestionAndAnswer = bool.Parse(config[key]);
                        break;
                }
            }
        }

        #region Properties

        public override string ApplicationName
        {
            get
            {
                return _ApplicationName;
            }
            set
            {
                _ApplicationName = value;
                //_CurrentStore = null;
            }
        }

        public override bool EnablePasswordReset
        {
            get { return _EnablePasswordReset; }
        }

        public override bool EnablePasswordRetrieval
        {
            get
            {
                if (this.PasswordFormat == MembershipPasswordFormat.Hashed)
                    return false;
                else
                    return true;
            }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { return _MaxInvalidPasswordAttempts; }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return _MinRequiredNonAlphanumericChars; }
        }

        public override int MinRequiredPasswordLength
        {
            get { return _MinRequiredPasswordLength; }
        }

        public override int PasswordAttemptWindow
        {
            get { return 20; }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { return _PasswordFormat; }
        }

        public override string PasswordStrengthRegularExpression
        {
            get
            {
                return _PasswordStrengthRegEx;
            }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { return _RequiresQuestionAndAnswer; }
        }

        public override bool RequiresUniqueEmail
        {
            get { return true; }
        }

        #endregion

        #region Methods

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            int czyZalogowac = HSOUserStore.Instance.ValidateUserInDomain(username.Remove(username.IndexOf('/')), Convert.ToInt32(username.Substring(username.IndexOf('/') + 1)), password, _ApplicationName, connectionString);
            if (czyZalogowac != 0)
                return true;
            else
                return false;
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        #endregion

        public int GetUserIdFromDatabase(string username, string password)
        {
            return HSOUserStore.Instance.ValidateUserInDomain(username.Remove(username.IndexOf('/')), Convert.ToInt32(username.Substring(username.IndexOf('/') + 1)), password, _ApplicationName, connectionString);
        }

        public MembershipUser GetUser(string username, string password)
        {
            int userID = HSOUserStore.Instance.ValidateUserInDomain(username.Remove(username.IndexOf('/')), Convert.ToInt32(username.Substring(username.IndexOf('/') + 1)), password, _ApplicationName, connectionString);
            MembershipUser usr = new MembershipUser("HSOMembershipProvider", userID.ToString(), userID,
                                                    "", "", "", true, false, DateTime.MinValue, DateTime.MinValue,
                DateTime.MinValue, DateTime.MinValue, DateTime.MaxValue);
            return usr;
        }

        public MembershipUser GetUserById(string username)
        {

            MembershipUser usr = new MembershipUser("HSOMembershipProvider", username, username,
                                                    "", "", "", true, false, DateTime.MinValue, DateTime.MinValue,
                DateTime.MinValue, DateTime.MinValue, DateTime.MaxValue);
            return usr;
        }

        public HSODomain[] GetDomainList()
        { 
            return HSODomainStore.Instance.CreateDomainList(_ApplicationName, connectionString);
        }

        public List<HSOBranch> GetBranchList(int idUser)
        { 
            return HSOBranchStore.Instance.GetBranchForUser(idUser , connectionString);
        }

        public List<HSOCompany> GetCompaniesForUser(string idUser)
        {
            return HSOCompanyStore.Instance.GetCompaniesForUser(idUser, connectionString);
        }

        public int ValidateUserInDomain(string _usrName, int _FirmaId, string _password)
        {
            return HSOUserStore.Instance.ValidateUserInDomain(_usrName, _FirmaId, _password, _ApplicationName, connectionString);
        }
    }
}
