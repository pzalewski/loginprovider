﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace HSOLoginProvider.Role
{
    internal class RoleStore
    {

        private static RoleStore engine = new RoleStore();

        //private RoleStore[] _RoleStore;

        private RoleStore()
        {

        }

        public static RoleStore Instance
        {
            get { return engine; }
        }

        public bool RoleExists(string roleName, string applicationName, string _connectionString)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(@"SELECT COUNT(*) FROM Uprawnienia u 
                                                    LEFT JOIN dbo.UprawnieniaAplikacje ua ON u.IdAplikacji = ua.IdAplikacji
                                                    WHERE u.nazwa = @roleName AND ua. = @applicationName", conn);
                cmd.Parameters.AddWithValue("roleName", roleName);
                cmd.Parameters.AddWithValue("applicationName", roleName);
                int ile = (int)cmd.ExecuteScalar();
                if (ile == 1)
                    return true;
                else
                    return false;
            }
        }

        public bool IsUserInRole(string username, string roleName, string applicationName, string _connectionString)
        {
            bool isInRole = false;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(@"SELECT COUNT(*) FROM dbo.UprawnieniaUzytkownika uu 
                                                    LEFT JOIN dbo.Uprawnienia u  ON uu.IdUprawnienia = u.IdUprawnienia
                                                    LEFT JOIN dbo.UprawnieniaAplikacje ua ON u.IdAplikacji = ua.IdAplikacji
                                                    WHERE uu.IdUser = @usrId 
                                                    AND u.Nazwa = @Nazwa", conn);
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("Nazwa", roleName);
                cmd.Parameters.AddWithValue("usrId", username);
                int ile = (int)cmd.ExecuteScalar();
                if (ile == 1)
                    isInRole = true;

                cmd.CommandText = @"SELECT COUNT(*) FROM dbo.UprawnieniaStanowiska us 
                                                    LEFT JOIN dbo.Uprawnienia u  ON us.IdUprawnienia = u.IdUprawnienia
                                                    LEFT JOIN dbo.Users usr ON us.IdStanowiska = usr.Stanowisko
                                                    WHERE usr.Id = @usrId AND u.Nazwa = @Nazwa";
                ile = (int)cmd.ExecuteScalar();
                if (ile == 1)
                    isInRole = true;

                    
            }
            return isInRole;
        }

        public string[] GetRolesForUser(string username, string applicationName, string _connectionString)
        {
            List<string> RoleList = new List<string>();
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                string[] userAndBranch = username.Split('#');
                conn.Open();
                SqlCommand cmd = new SqlCommand("HSOLoginProviderGetUserRoles", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("IdUser", userAndBranch[0]);
                cmd.Parameters.AddWithValue("ApplicationName", applicationName);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable tmp = new DataTable();
                da.Fill(tmp);
                foreach (DataRow r in tmp.Rows)
                {
                    if (!RoleList.Contains(r["Name"].ToString()))
                        RoleList.Add(r["Name"].ToString());
                }

                cmd.CommandText = "HSOLoginProviderGetJobRoles";
                
                if(userAndBranch.Count() > 1)
                    cmd.Parameters.AddWithValue("IdBranch", userAndBranch[1]);
                tmp.Rows.Clear();
                da.Fill(tmp);
                foreach (DataRow r in tmp.Rows)
                {
                    if (!RoleList.Contains(r["Name"].ToString()))
                        RoleList.Add(r["Name"].ToString());
                }


            }
            return RoleList.ToArray();
        }

        public string[] GetAllRoles(string applicationName, string _connectionString)
        {
            List<string> RoleList = new List<string>();
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("HSOLoginProviderGetAllRoles", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("ApplicationName", applicationName);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable tmp = new DataTable();
                da.Fill(tmp);
                foreach (DataRow r in tmp.Rows)
                {
                    if (!RoleList.Contains(r["Name"].ToString()))
                        RoleList.Add(r["Name"].ToString());
                }
            }
            return RoleList.ToArray();
        }

        public Dictionary<string, string> GetAllRolesWithDescription(string applicationName, string _connectionString)
        {
            Dictionary<string, string> RoleList = new Dictionary<string,string>();
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("HSOLoginProviderGetAllRoles", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("ApplicationName", applicationName);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable tmp = new DataTable();
                da.Fill(tmp);
                foreach (DataRow r in tmp.Rows)
                {
                    if (!RoleList.Contains(new KeyValuePair<string,string>(r["Name"].ToString(), r["Description"].ToString())))
                        RoleList.Add(r["Name"].ToString(), r["Description"].ToString());
                }
            }
            return RoleList;
        }

        public void AddUsersToRoles(string[] usernames, string[] roleNames, string _connectionString)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                foreach (String username in usernames)
                {
                    foreach (String rolename in roleNames)
                    {
                        SqlCommand cmd = new SqlCommand("HSOLoginProviderAddUserToRole", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("IdUser", username);
                        cmd.Parameters.AddWithValue("rolename", rolename);
                        cmd.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }
        }

        public void RemoveUsersFromRoles(string[] usernames, string[] roleNames, string _connectionString)
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                foreach (String username in usernames)
                {
                    foreach (String rolename in roleNames)
                    {
                        SqlCommand cmd = new SqlCommand("HSOLoginProviderRemoveUserToRole", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("IdUser", username);
                        cmd.Parameters.AddWithValue("rolename", rolename);
                        cmd.ExecuteNonQuery();
                    }
                }
                conn.Close();
            }
        }
    }
}
