﻿using System;
using System.Web.Security;
using HSOLoginProvider.Role;
using System.Collections.Specialized;
using System.Configuration;
using System.Web;
using System.Collections.Generic;

namespace HSOLoginProvider
{
    public class HSORoleProvider : RoleProvider
    {
        private string _ApplicationName;
        private string _connectionString;

        public override void Initialize(string name, NameValueCollection config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            if (string.IsNullOrEmpty(name))
            {
                name = "HSORoleProvider";
            }
            if (string.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "HSO Role Provider");
            }

            // Base initialization
            base.Initialize(name, config);

            // Initialize properties
            _ApplicationName = "DefaultApp";
            foreach (string key in config.Keys)
            {
                if (key.ToLower().Equals("applicationname"))
                    ApplicationName = config[key];
                
            }

            ConnectionStringSettings connectionStringSettings = ConfigurationManager.
            ConnectionStrings[config["connectionStringName"]];
            if (connectionStringSettings == null ||
                connectionStringSettings.ConnectionString.Trim() == "")
            {
                throw new HttpException("You must supply a connection string.");
            }
            else
            {
                _connectionString = connectionStringSettings.ConnectionString;
            }

        }

        #region Properties

        public override string ApplicationName
        {
            get
            {
                return _ApplicationName;
            }
            set
            {
                _ApplicationName = value;
            }
        }

        public string ConnectionStringName { get; set; }
        

        #endregion

        #region Methods

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            try
            {
                return RoleStore.Instance.RoleExists(roleName, _ApplicationName, _connectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            try
            {
                RoleStore.Instance.AddUsersToRoles(usernames, roleNames, _connectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            try
            {
                RoleStore.Instance.RemoveUsersFromRoles(usernames, roleNames, _connectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override string[] GetAllRoles()
        {
            try
            {
                return RoleStore.Instance.GetAllRoles(_ApplicationName, _connectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override string[] GetRolesForUser(string username)
        {
            try
            {
                return RoleStore.Instance.GetRolesForUser(username, _ApplicationName, _connectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            try
            {
                return RoleStore.Instance.IsUserInRole(username, roleName, _ApplicationName, _connectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        #endregion

        public Dictionary<string, string> GetAllRolesWithDescription()
        {
            try
            {
                return RoleStore.Instance.GetAllRolesWithDescription(_ApplicationName, _connectionString);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

}
