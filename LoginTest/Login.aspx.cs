﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

namespace LoginTest
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                HSOLoginProvider.HSOMembershipUser memb = (HSOLoginProvider.HSOMembershipUser)Membership.Providers["HSOMembershipProvider"];
                HSOLoginProvider.Membership.HSODomain[] d = memb.GetDomainList();
                ddlDirectory.DataSource = d;
                ddlDirectory.DataTextField = "DomainName";
                ddlDirectory.DataValueField = "DomainId";
                ddlDirectory.DataBind();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Membership.ValidateUser(tbxLogin.Text.Trim() + "/" + ddlDirectory.SelectedValue, tbxPassword.Text))
            {
                
                HSOLoginProvider.HSOMembershipUser memb = (HSOLoginProvider.HSOMembershipUser)Membership.Providers["HSOMembershipProvider"];
                int IdUser = memb.GetUserIdFromDatabase(tbxLogin.Text.Trim() + "/" + ddlDirectory.SelectedValue, tbxPassword.Text);
                Session["IdUser"] = IdUser;
                List<HSOLoginProvider.Membership.HSOBranch> branches = memb.GetBranchList(IdUser);
                List<HSOLoginProvider.Membership.HSOCompany> companies = memb.GetCompaniesForUser(IdUser.ToString());
                if (branches.Count > 1)
                {

                    ddlBranch.DataSource = branches;
                    ddlBranch.DataTextField = "Name";
                    ddlBranch.DataValueField = "IdBranch";
                    ddlBranch.DataBind();
                    modalChoseYourBranch.Show();
                }
                else
                {
                    string returnUrl = Request.QueryString["ReturnUrl"];
                    if (returnUrl == null) returnUrl = "~/Default.aspx";
                    MembershipUser usr = (MembershipUser)memb.GetUserById(IdUser.ToString());
                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(usr.UserName, true, 20);
                    string hashCookies = FormsAuthentication.Encrypt(ticket);

                    HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hashCookies); // Hashed ticket

                    // Add the cookie to the response, user browser

                    Response.Cookies.Add(cookie);
                    Authorize();
                    Response.Redirect(returnUrl);
                }
            }
        }

        protected void Authorize()
        {
            UserProfile profile;
            User.IsInRole("test");
            Roles.GetRolesForUser(Page.User.Identity.Name);

            //string[] role = Roles.GetRolesForUser(usr.UserName); 
        }

        protected void btnChoseBranch_Click(object sender, EventArgs e)
        {
            if (Session["IdUser"] != null)
            {
                string username = Session["IdUser"].ToString();
                HSOLoginProvider.HSOMembershipUser memb = (HSOLoginProvider.HSOMembershipUser)Membership.Providers["HSOMembershipProvider"];
                MembershipUser usr = (MembershipUser)memb.GetUserById(username + "#" + ddlBranch.SelectedValue);

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(usr.UserName, true, 20);
                string hashCookies = FormsAuthentication.Encrypt(ticket);

                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hashCookies); // Hashed ticket

                // Add the cookie to the response, user browser

                Response.Cookies.Add(cookie);
                string returnUrl = Request.QueryString["ReturnUrl"];
                if (returnUrl == null) returnUrl = "~/Default.aspx";
                Authorize();
                Response.Redirect(returnUrl);
            }
        }
    }
}