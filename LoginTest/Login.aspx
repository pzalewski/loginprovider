﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="LoginTest.Login" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

</head>
<body>
    <form id="form1" runat="server">
        <asp:scriptmanager ID="Scriptmanager1" runat="server"></asp:scriptmanager>
        <div>
            <asp:TextBox ID="tbxLogin" runat="server"></asp:TextBox><br />
            <asp:TextBox ID="tbxPassword" TextMode="Password" runat="server"></asp:TextBox><br />
            <asp:DropDownList ID="ddlDirectory" runat="server">
            
            </asp:DropDownList><br />
            <asp:Button ID="Button1" runat="server" Text="Button" onclick="Button1_Click" />
        </div>

        <asp:Panel ID="PnlWyborOddzialu" runat="server">Wybierz oddział:<asp:DropDownList ID="ddlBranch" runat="server">
            
            </asp:DropDownList><br />
            <asp:Button ID="btnChoseBranch" runat="server" Text="Wybierz" 
                onclick="btnChoseBranch_Click" />
            <asp:Button ID="btnClose" runat="server" Text="Anuluj" />
        </asp:Panel>
         

        <asp:LinkButton ID="LinkButton1" style="display:none" runat="server">LinkButton</asp:LinkButton>
        <cc1:ModalPopupExtender ID="modalChoseYourBranch" PopupControlID="PnlWyborOddzialu" 
            BackgroundCssClass="modalBackground" runat="server" 
        TargetControlID="LinkButton1" CancelControlID="btnClose" DynamicServicePath="" 
        Enabled="True">
        </cc1:ModalPopupExtender>
    </form>
</body>
</html>


