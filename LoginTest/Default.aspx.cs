﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Security.Permissions;
using System.Web.Profile;

namespace LoginTest
{
    public partial class _Default : System.Web.UI.Page
    {
        //[PrincipalPermissionAttribute(SecurityAction.Demand, Role = "Delegacja krajowa - INICJACJA")]
        protected void Page_Load(object sender, EventArgs e)
        {
            UserProfile profile = UserProfile.GetUserProfile(User.Identity.Name);
            string dzial = profile.Dzial;
            string stanowisko = profile.Stanowisko;
            User.IsInRole("test");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();

            string username = "333014502";
            HttpCookie authCookie = LoginEngine.Instance.GetLogOnUserCookieById(username);

            Response.Cookies.Add(authCookie);

            UserInfo usr = LoginEngine.Instance.AuthorizeUser(username);

            Session["user"] = usr;
            Response.Redirect("~/Default.aspx");
        }
    }   
}
