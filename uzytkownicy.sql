USE [Uzytkownicy]
GO
/****** Object:  Table [dbo].[CurrentStructure]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CurrentStructure](
	[CurrentStructure] [xml] NULL,
	[IdCompany] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangeInStructure]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChangeInStructure](
	[IdChangeInStructure] [int] IDENTITY(1,1) NOT NULL,
	[Data] [xml] NULL,
	[Date] [datetime] NULL,
	[IdCompany] [int] NULL,
	[Filename] [varchar](50) NULL,
 CONSTRAINT [PK_ZmianyWStrukturze] PRIMARY KEY CLUSTERED 
(
	[IdChangeInStructure] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Structure]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Structure](
	[IdDepartment] [int] NOT NULL,
	[IdParent] [int] NULL,
	[IdCompany] [int] NULL,
 CONSTRAINT [PK_Structure] PRIMARY KEY CLUSTERED 
(
	[IdDepartment] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[p_UsunUsera]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_UsunUsera]
@PersonRCPID INT,
@Firma INT

AS

    IF @Firma = 1 
        BEGIN
            SET @PersonRCPID = @PersonRCPID + 111000000
        END
    ELSE 
        BEGIN
            SET @PersonRCPID = @PersonRCPID + 222000000
        END
        
UPDATE Users SET aktywny = 0, DataOstatniejZmiany = GETDATE()  WHERE ID = @PersonRCPID
GO
/****** Object:  StoredProcedure [dbo].[p_test]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[p_test]

AS

DECLARE @ile AS INT 

SET @ile = 100 

SELECT * FROM users WHERE id = @ile
GO
/****** Object:  StoredProcedure [dbo].[p_utworzStrukture]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_utworzStrukture]
@Nazwa VARCHAR(200),
@Rodzaj VARCHAR(50),
@Nadrzedne VARCHAR(200),
@RodzajNadrzedny VARCHAR(50),
@Region VARCHAR(200),
@OddzialNadrzedny VARCHAR(200)

AS

DECLARE @Id AS INT,
@IdNadrzedne AS INT,
@Licznik AS INT,
@IdRegionu AS INT,
@IdOddzialuNadrzednego AS INT


SET @Id = (SELECT IdObiektu FROM WlasciwosciObiektu WHERE Wartosc = @Nazwa AND IdTypuWlasciwosci = 1)
SET @IdNadrzedne = (SELECT IdObiektu FROM WlasciwosciObiektu WHERE Wartosc = @Nadrzedne AND IdTypuWlasciwosci = 1)
IF @Nazwa = 'Zarząd'
BEGIN
	SET @IdNadrzedne = 0
END

IF @Id IS NOT NULL AND @IdNadrzedne IS NOT NULL
BEGIN
	SET @Licznik = (SELECT COUNT(IdObiektu) FROM Struktura WHERE IdObiektu = @Id AND IdObiektuNadrzednego = @IdNadrzedne)
	IF @Licznik = 0 
	BEGIN
		INSERT INTO Struktura (IdObiektu, IdObiektuNadrzednego) VALUES (@Id, @IdNadrzedne)
		IF @Rodzaj = 'Oddział'
		BEGIN
			IF @Region != ''
			BEGIN
				SET @IdRegionu = (SELECT IdObiektu FROM WlasciwosciObiektu WHERE Wartosc = @Region AND IdTypuWlasciwosci = 1)
				INSERT INTO WlasciwosciObiektu (IdObiektu, IdTypuWlasciwosci, Wartosc) VALUES (@Id, 4, @IdRegionu)
			END
			IF @OddzialNadrzedny != ''
			BEGIN
				SET @IdOddzialuNadrzednego = (SELECT IdObiektu FROM WlasciwosciObiektu WHERE Wartosc = @OddzialNadrzedny AND IdTypuWlasciwosci = 1)
				INSERT INTO WlasciwosciObiektu (IdObiektu, IdTypuWlasciwosci, Wartosc) VALUES (@Id, 3, @IdOddzialuNadrzednego)
			END
		END
	END	
END
GO
/****** Object:  StoredProcedure [dbo].[p_UstalKierownika]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_UstalKierownika]
    @IdUsera INT,
    @KierownikImie VARCHAR(50),
    @KierownikNazwisko VARCHAR(50),
    @Firma INT
AS 

    IF @Firma = 1 
        BEGIN
            SET @IdUsera = @IdUsera + 111000000
        END
    ELSE 
        BEGIN
            SET @IdUsera = @IdUsera + 222000000
        END
        
    UPDATE  Users
    SET     Kierownik = ( SELECT TOP 1 Id
                          FROM      Users
                          WHERE     Imie = @KierownikImie
                                    AND Nazwisko = @KierownikNazwisko AND Firma = @Firma
                        )
    WHERE   Id = @IdUsera AND Firma = @Firma
GO
/****** Object:  StoredProcedure [dbo].[p_DrzewoStruktury]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_DrzewoStruktury]
@IdNadrzedne INT

AS

DECLARE @IdRegionu INT, @CzyPoddrzewo BIT

create table #tree (id int, sequence varchar(1000), levelNo int)
insert #tree select Struktura.idObiektu, right(space(10) + convert(varchar(10),Struktura.idObiektu),10), 1 from Struktura where Struktura.IdObiektuNadrzednego = @IdNadrzedne
declare @i int
select @i = 0
while @@rowcount > 0
begin
	 select @i = @i + 1 
	 


     insert #tree
     -- Get all children of previous level
     select Struktura.idObiektu, sequence + right(space(10) + convert(varchar(10),Struktura.idObiektu),10), @i + 1
     from Struktura, #tree
     where #tree.levelNo = @i
     and Struktura.IdObiektuNadrzednego = #tree.id
end

-- output with hierarchy formatted
select dbo.Jednostki.ID, space((levelNo-1)*4) + dbo.Jednostki.Nazwa AS dzial, COUNT(*)
from #tree, dbo.Jednostki, dbo.Users
where #tree.id = dbo.Jednostki.ID 
	AND dbo.Jednostki.ID = dbo.Users.Dzial
	AND dbo.Users.aktywny = 1
GROUP BY dbo.Jednostki.ID, #tree.levelNo, dbo.Jednostki.Nazwa, #tree.sequence
order by sequence
GO
/****** Object:  StoredProcedure [dbo].[p_Drzewo]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_Drzewo]

AS


CREATE TABLE #tree(
Id INT IDENTITY (1,1),
IdObiektu INT,
Nazwa VARCHAR(200),
IdNadrzedne INT,
NazwaNadrzedna VARCHAR(200),
TypObiektu VARCHAR(50)
)



INSERT INTO #tree (IdObiektu, Nazwa, IdNadrzedne, NazwaNadrzedna, TypObiektu)(
SELECT s.IdObiektu, w.Wartosc, s.IdObiektuNadrzednego, w1.Wartosc, o.TypObiektu 
FROM Struktura s LEFT JOIN WlasciwosciObiektu w ON s.IdObiektu = w.IdObiektu AND w.IdTypuWlasciwosci = 1
LEFT JOIN WlasciwosciObiektu w1 ON s.IdObiektuNadrzednego = w1.IdObiektu AND w1.IdTypuWlasciwosci = 1
LEFT JOIN Obiekty o ON s.IdObiektu = o.IdObiektu WHERE s.IdObiektuNadrzednego = 0)

SELECT IdObiektu FROM #tree

DROP TABLE #tree

SELECT * FROM #tree WHERE NazwaNadrzedna = 'Zarząd'
GO
/****** Object:  StoredProcedure [dbo].[p_UprawnieniaPracownika]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_UprawnieniaPracownika] @IdUser INT

AS

DECLARE @IdStanowiska AS INT, 
		@IdJednostki AS INT

SET @IdStanowiska = (SELECT Stanowisko FROM Users WHERE ID = @IdUser)
SET @IdJednostki = (SELECT Dzial FROM Users WHERE ID = @IdUser)

CREATE TABLE #tmp
(
Id INT IDENTITY,
IdUprawnienia  INT
)
--uprawnienia dla uzytkownika
INSERT INTO #tmp (IdUprawnienia) (SELECT IdUprawnienia FROM UprawnieniaUzytkownika WHERE IdUser = @IdUser)
--INSERT INTO #tmp (IdUprawnienia) (SELECT u.IdUprawnienia FROM UprawnieniaSzablonUzytkownika us LEFT JOIN UprawnieniaSzablonSkladowe u ON us.IdSzablonu = u.IdSzablonu WHERE us.IdUser = @IdUser)

--uprawnienia dla stanowiska
INSERT INTO #tmp (IdUprawnienia) (SELECT IdUprawnienia FROM UprawnieniaStanowiska WHERE IdStanowiska = @IdStanowiska)
--INSERT INTO #tmp (IdUprawnienia) (SELECT u.IdUprawnienia FROM UprawnieniaSzablonStanowiska us LEFT JOIN UprawnieniaSzablonSkladowe u ON us.IdSzablonu = u.IdSzablonu WHERE us.IdStanowiska = @IdStanowiska)

SELECT IdUprawnienia FROM #tmp GROUP BY IdUprawnienia

DROP TABLE #tmp
GO
/****** Object:  StoredProcedure [dbo].[sp_BackupBazy]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_BackupBazy]

AS


-- Create the backup device for the full MyNwind backup.

--USE master

DECLARE @path AS VARCHAR(200)

SET @path = 'c:\_public\piotr_z\uzytkownicySyriuszBackup'+CONVERT(varchar,GETDATE(),102)+'.bak'

print @path

EXEC sp_addumpdevice 'disk', 'UzytkownicyBackup', @path

  

-- Back up the full MyNwind database.

BACKUP DATABASE Uzytkownicy TO UzytkownicyBackup

EXEC sp_dropdevice 'UzytkownicyBackup'
GO
/****** Object:  StoredProcedure [dbo].[p_zmienStrukture]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_zmienStrukture]

@Nazwa VARCHAR(200),
@Rodzaj VARCHAR(50),
@Nadrzedne VARCHAR(200),
@RodzajNadrzedny VARCHAR(50),
@NadrzednyZmieniony VARCHAR(200),
@RodzajNadrzednyZmieniony VARCHAR(50)

AS

DECLARE @Id AS INT,
@IdNadrzedne AS INT,
@IdNadrzedneZmieniony AS INT,
@Licznik AS INT

SET @Id = (SELECT IdObiektu FROM WlasciwosciObiektu WHERE IdTypuWlasciwosci =1 AND Wartosc = @Nazwa)
SET @IdNadrzedne = (SELECT IdObiektu FROM WlasciwosciObiektu WHERE IdTypuWlasciwosci =1 AND Wartosc = @Nadrzedne)
SET @IdNadrzedneZmieniony = (SELECT IdObiektu FROM WlasciwosciObiektu WHERE IdTypuWlasciwosci =1 AND Wartosc = @NadrzednyZmieniony)
GO
/****** Object:  StoredProcedure [dbo].[p_WyszukajOddzial]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_WyszukajOddzial]
@NumerPk varchar(100),
@NazwaPlacowki varchar(100),
@Region varchar(100)
AS
BEGIN
SELECT p.nr_pk, p.IdRegionu, p.Nazwa_pk, p.KodPocztowy + ' ' + p.Miasto AS Miasto, p.Ulica + ' ' +p.nr_domu AS Address, p.Telefon + ' <br />' + p.AdresEmail AS DaneKontaktowe, p.Aktywny, r.NazwaRegionu FROM NazwyPlacowek p 
		INNER JOIN NazwyRegionow r ON p.IdRegionu = r.IdRegionu
--		INNER JOIN Oddzialy o ON p.nr_pk = o.Numer
		WHERE p.Aktywny = '1'
				AND
			(p.nr_pk LIKE @NumerPk + '%%') 
				AND
			(p.Nazwa_pk LIKE @NazwaPlacowki + '%%')
				AND
			(p.IdRegionu LIKE @Region + '%%')
		ORDER BY p.nr_pk ASC
END
GO
/****** Object:  StoredProcedure [dbo].[p_Dok_tech_ListaOddzialow_Pobierz]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_Dok_tech_ListaOddzialow_Pobierz] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT Numer, Nazwa FROM dbo.Oddzialy
END
GO
/****** Object:  Table [dbo].[Application]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Application](
	[IdApplication] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[Description] [text] NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_UprawnieniaAplikacje] PRIMARY KEY CLUSTERED 
(
	[IdApplication] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[p_PobierzKierownikow]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_PobierzKierownikow] @IdUser INT

AS 

DECLARE @idKierownika AS INT
CREATE TABLE #tmp
(
Id INT,
Imie VARCHAR(50),
Nazwisko VARCHAR(200)
)

SET @idKierownika = (SELECT kierownik FROM Users WHERE Id = @IdUser)

WHILE @idKierownika IS NOT NULL
BEGIN
	INSERT INTO #tmp (Id, Imie, Nazwisko) (SELECT id, Imie, Nazwisko FROM Users WHERE Id = @IdKierownika)
	SET @idKierownika = (SELECT kierownik FROM Users WHERE Id = @idKierownika)
END


SELECT * FROM #tmp
GO
/****** Object:  StoredProcedure [dbo].[p_UprawnieniaPracownikaDlaAplikacji]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_UprawnieniaPracownikaDlaAplikacji] @IdUser INT, @IdAplikacji INT
AS 
    DECLARE @IdStanowiska AS INT,
        @IdJednostki AS INT

    SET @IdStanowiska = ( SELECT    Stanowisko
                          FROM      Users
                          WHERE     ID = @IdUser
                        )
    SET @IdJednostki = ( SELECT Dzial
                         FROM   Users
                         WHERE  ID = @IdUser
                       )

    CREATE TABLE #tmp
        (
          Id INT IDENTITY,
          IdUprawnienia INT
        )
--uprawnienia dla uzytkownika
    INSERT  INTO #tmp ( IdUprawnienia )
            ( SELECT    uu.IdUprawnienia
              FROM      UprawnieniaUzytkownika uu
              LEFT JOIN dbo.Uprawnienia u ON uu.IdUprawnienia = u.IdUprawnienia
              WHERE     uu.IdUser = @IdUser AND u.IdAplikacji = @IdAplikacji
            )
    --INSERT  INTO #tmp ( IdUprawnienia )
    --        ( SELECT    uss.IdUprawnienia
    --          FROM      UprawnieniaSzablonUzytkownika us
    --                    LEFT JOIN UprawnieniaSzablonSkladowe uss ON us.IdSzablonu = uss.IdSzablonu
    --                    LEFT JOIN dbo.Uprawnienia u ON uss.IdUprawnienia = u.IdUprawnienia
    --          WHERE     us.IdUser = @IdUser AND u.IdAplikacji = @IdAplikacji
    --        )

--uprawnienia dla stanowiska
    INSERT  INTO #tmp ( IdUprawnienia )
            ( SELECT    us.IdUprawnienia
              FROM      UprawnieniaStanowiska us
              LEFT JOIN dbo.Uprawnienia u ON us.IdUprawnienia = u.IdUprawnienia
              WHERE     us.IdStanowiska = @IdStanowiska AND u.IdAplikacji = @IdAplikacji
            )
    --INSERT  INTO #tmp ( IdUprawnienia )
    --        ( SELECT    uss.IdUprawnienia
    --          FROM      UprawnieniaSzablonStanowiska us
    --                    LEFT JOIN UprawnieniaSzablonSkladowe uss ON us.IdSzablonu = uss.IdSzablonu
    --                    LEFT JOIN dbo.Uprawnienia u ON uss.IdUprawnienia = u.IdUprawnienia
    --          WHERE     us.IdStanowiska = @IdStanowiska AND u.IdAplikacji = @IdAplikacji
    --        )

--uprawnienia dla jednostki
    --INSERT  INTO #tmp ( IdUprawnienia )
    --        ( SELECT    IdUprawnienia
    --          FROM      UprawnieniaJednostki
    --          WHERE     IdJednostki = @IdJednostki
    --        )
    --INSERT  INTO #tmp ( IdUprawnienia )
    --        ( SELECT    u.IdUprawnienia
    --          FROM      UprawnieniaSzablonJednostki us
    --                    LEFT JOIN UprawnieniaSzablonSkladowe u ON us.IdSzablonu = u.IdSzablonu
    --          WHERE     us.IdJednostki = @IdJednostki
    --        )

    SELECT  IdUprawnienia
    FROM    #tmp
    GROUP BY IdUprawnienia

    DROP TABLE #tmp
GO
/****** Object:  StoredProcedure [dbo].[p_WyswietlJednostkiPodrzedne]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_WyswietlJednostkiPodrzedne] @IdJednostki INT

AS


create table #tree (id int, levelNo int)
insert #tree select Struktura.idObiektu, 1 from Struktura where Struktura.IdObiektu = @IdJednostki
declare @i int
select @i = 0
while @@rowcount > 0
begin
	 select @i = @i + 1 
	 


     insert #tree
     -- Get all children of previous level
     select Struktura.idObiektu, @i + 1
     from Struktura, #tree
     where #tree.levelNo = @i
     and Struktura.IdObiektuNadrzednego = #tree.id
end

-- output with hierarchy formatted

SELECT * FROM #tree
GO
/****** Object:  StoredProcedure [dbo].[p_FirmyPobierz]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_FirmyPobierz]
	
AS
BEGIN
	SELECT * FROM dbo.Firma
		WHERE Aktywny = 1
END
GO
/****** Object:  StoredProcedure [dbo].[p_PobierzListePracownikow]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_PobierzListePracownikow] @IdUser INT

AS

SELECT 
	ID,
	Nazwisko + ' ' + Imie AS 'ImieNazwisko'
FROM Users 
WHERE Kierownik = @IdUser AND aktywny = 1 ORDER BY Nazwisko ASC
GO
/****** Object:  Table [dbo].[HistoriaUsers]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HistoriaUsers](
	[id] [int] NULL,
	[login] [varchar](50) NULL,
	[Imie] [varchar](50) NULL,
	[Nazwisko] [varchar](50) NULL,
	[DataOstatniejZmianyHasla] [datetime] NULL,
	[windkorp] [varchar](50) NULL,
	[wpis_data] [datetime] NULL,
	[wpis_osoba] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[v_StrukturaPodleglosciStefczyka]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_StrukturaPodleglosciStefczyka]
AS
SELECT     ID, Kierownik, SimpleID
FROM         dbo.Users
WHERE     (Firma = 1) AND (aktywny = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 294
               Right = 255
            End
            DisplayFlags = 280
            TopColumn = 3
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_StrukturaPodleglosciStefczyka'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_StrukturaPodleglosciStefczyka'
GO
/****** Object:  View [dbo].[v_StrukturaPodleglosciTZSKA]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_StrukturaPodleglosciTZSKA]
AS
SELECT     ID, Kierownik, SimpleID
FROM         dbo.Users
WHERE     (Firma = 2) AND (aktywny = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 216
               Right = 293
            End
            DisplayFlags = 280
            TopColumn = 6
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_StrukturaPodleglosciTZSKA'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_StrukturaPodleglosciTZSKA'
GO
/****** Object:  StoredProcedure [dbo].[p_drzewoPodleglosciPracownikow]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_drzewoPodleglosciPracownikow] @Id INT
AS

DECLARE @IdRegionu INT, @CzyPoddrzewo BIT

create table #tree (id int, sequence varchar(1000), levelNo int)
insert #tree select dbo.Users.id, right(space(10) + convert(varchar(10),dbo.Users.id),10), 1 from dbo.Users where dbo.Users.Kierownik = @Id
declare @i int
select @i = 0
while @@rowcount > 0
begin
	 select @i = @i + 1 
	 


     insert #tree
     -- Get all children of previous level
     select dbo.Users.id, sequence + right(space(10) + convert(varchar(10),dbo.Users.id),10), @i + 1
     from dbo.Users, #tree
     where #tree.levelNo = @i
     and dbo.Users.Kierownik = #tree.id
end

--SELECT * FROM #tree

SELECT dbo.Users.ID, space((levelNo-1)*4) + dbo.Users.Nazwisko + ' ' + dbo.Users.Imie, dbo.Users.SimpleID
FROM #tree, dbo.Users
WHERE #tree.id = dbo.Users.ID
GROUP BY dbo.Users.ID, #tree.levelNo, dbo.Users.Nazwisko,dbo.Users.Imie, dbo.Users.SimpleID , #tree.sequence
order by sequence

DROP TABLE #tree
GO
/****** Object:  StoredProcedure [dbo].[p_WindKorpObsluga]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_WindKorpObsluga]

@opcja INT,
@login varchar(20)=NULL,
@nrTel varchar(20)=null

AS

BEGIN


--pobieram tel
IF @opcja=1
  BEGIN
	SELECT [login],Imie +' '+Nazwisko AS ImieNazwisko,nrTel,windkorp,aktywny FROM dbo.Users 
	WHERE (id between 777000000 AND 778000000) AND aktywny=1 AND login NOT IN('bdrewniak','test','test2') ORDER BY login 
  END	


--zmieniam tel
IF @opcja=2
  BEGIN
	UPDATE dbo.Users SET nrTel=@nrTel
	WHERE [login]=@login AND id between 777000000 AND 778000000 
  END	

	
END
GO
/****** Object:  Table [dbo].[Company]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Company](
	[IdCompany] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[IsActive] [bit] NULL,
	[Preffix] [int] NULL,
	[Adress] [varchar](2000) NULL,
	[Suffix] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[IsCreditUnion] [bit] NULL,
	[LDAP] [varchar](500) NULL,
	[DGAImport] [bit] NOT NULL,
	[NoSKOK] [int] NULL,
	[IsBrazilianModel] [bit] NOT NULL,
 CONSTRAINT [PK_Firma] PRIMARY KEY CLUSTERED 
(
	[IdCompany] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[Firma]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Firma]
AS
SELECT     IdCompany AS FirmaId, Name AS Nazwa, isActive AS Aktywny, Preffix, Adress AS Adres, Suffix, Email AS mail, isCreditUnion AS CzyKasa, LDAP
FROM         dbo.Company
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Company"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 301
               Right = 440
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Firma'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Firma'
GO
/****** Object:  View [dbo].[Oddzialy]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Oddzialy]
AS
SELECT     IdBranch AS Id, Name AS Nazwa, NoBranch AS Numer, CreateDate AS DataUtworzenia, ChangeDate AS DataOstatniejZmiany, isActive AS Aktywny, 
                      IdCompany AS Firma, IdRegion AS Region
FROM         dbo.Branch
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Branch"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 286
               Right = 277
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Oddzialy'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Oddzialy'
GO
/****** Object:  View [dbo].[Struktura]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Struktura]
AS
SELECT     IdDepartment AS IdObiektu, IdParent AS IdObiektuNadrzednego, IdCompany AS Firma
FROM         dbo.Structure
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Structure"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 110
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Struktura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Struktura'
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_CreateBranchFromStructure]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_CreateBranchFromStructure] 
	@IdBranch INT,
    @Name VARCHAR(500),
    @NoBranch INT,
    @IdCompany INT,
    @IdRegion INT,
    @IdParent INT = NULL,
    @IsPK BIT = NULL, 
    @NoSKOK INT
AS 

 
 INSERT INTO [dbo].Branch
        ( IdBranch ,
          Name ,
          NoBranch ,
          CreateDate ,
          IsActive ,
          IdCompany ,
          IdRegion,
          IdParent,
          IsPK,
          NoSKOK
        )
     VALUES
           (@IdBranch
           ,@Name
           ,@NoBranch
           ,GETDATE()
           ,1
           ,@IdCompany
           ,@IdRegion
           ,@IdParent
		   ,@IsPK
		   ,@NoSKOK)
GO
/****** Object:  UserDefinedFunction [dbo].[RemoveNonAlphaCharacters]    Script Date: 02/27/2014 07:00:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Function [dbo].[RemoveNonAlphaCharacters](@Temp VarChar(1000))
Returns VarChar(1000)
AS
Begin

    Declare @KeepValues as varchar(50)
    SET @KeepValues = '%[^a-ż]%'
    While PatIndex(@KeepValues, @Temp) > 0
        Set @Temp = Stuff(@Temp, PatIndex(@KeepValues, @Temp), 1, '')

    Return @Temp
End
GO
/****** Object:  Table [dbo].[tmp]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tmp](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nazwisko] [varchar](500) NULL,
	[imie] [varchar](50) NULL,
	[uprawnienie] [int] NULL,
	[iduser] [int] NULL,
 CONSTRAINT [PK_tmp] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tmpOddzial]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmpOddzial](
	[id] [int] NOT NULL,
	[nazwa] [nvarchar](500) NULL,
	[numer] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tmpUser]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmpUser](
	[id] [int] NULL,
	[imie] [nvarchar](500) NULL,
	[nazwisko] [nvarchar](500) NULL,
	[stanowisko] [nvarchar](500) NULL,
	[oddzial] [nvarchar](500) NULL,
	[nroddzial] [int] NULL,
	[idoddzial] [int] NULL,
	[idstanowisko] [int] NULL,
	[login] [nvarchar](50) NULL,
	[haslo] [nvarchar](50) NULL,
	[czySF] [bit] NULL,
	[czyKS] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_UpdateBranch]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_UpdateBranch] 
	@IdBranch INT,
    @Name VARCHAR(500),
    @NoBranch INT,
	@IsActive BIT,
    @IdRegion INT,
    @IdParent INT,
    @IsPK BIT,
    @NoSKOK INT
    
AS 

UPDATE [dbo].[Branch]
   SET [Name] = @Name 
      ,[NoBranch] = @NoBranch
      ,[ChangeDate] = GETDATE()
      ,[IsActive] = @IsActive
      ,[IdRegion] = @IdRegion
      ,[IdParent] = @IdParent
      ,[IsPK] = @IsPK
      ,[NoSKOK] = @NoSKOK
 WHERE IdBranch = @IdBranch
GO
/****** Object:  Table [dbo].[tmpStanowisko]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tmpStanowisko](
	[id] [int] NULL,
	[nazwa] [nvarchar](500) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Region]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Region](
	[IdRegion] [int] NOT NULL,
	[Name] [varchar](500) NULL,
	[CreateDate] [datetime] NULL,
	[ChangeDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IdCompany] [int] NULL,
 CONSTRAINT [PK_Regiony] PRIMARY KEY CLUSTERED 
(
	[IdRegion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Branch](
	[IdBranch] [int] NOT NULL,
	[Name] [varchar](500) NULL,
	[NoBranch] [int] NOT NULL,
	[CreateDate] [datetime] NULL,
	[ChangeDate] [datetime] NULL,
	[IsActive] [bit] NOT NULL,
	[IdCompany] [int] NULL,
	[IdRegion] [int] NULL,
	[IdParent] [int] NULL,
	[IsPK] [bit] NOT NULL,
	[NoSKOK] [int] NOT NULL,
 CONSTRAINT [PK_Oddzialy] PRIMARY KEY CLUSTERED 
(
	[IdBranch] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Department]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Department](
	[IdDepartment] [int] NOT NULL,
	[Name] [varchar](500) NULL,
	[CreateDate] [datetime] NULL,
	[ChangeDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IdCompany] [int] NULL,
 CONSTRAINT [PK_Jednostki] PRIMARY KEY CLUSTERED 
(
	[IdDepartment] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Job]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Job](
	[IdJob] [int] NOT NULL,
	[Name] [varchar](500) NULL,
	[CreateDate] [datetime] NULL,
	[ChangeDate] [datetime] NULL,
	[IsActive] [bit] NULL,
	[IdCompany] [int] NULL,
 CONSTRAINT [PK_Stanowiska] PRIMARY KEY CLUSTERED 
(
	[IdJob] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[IdUser] [int] NOT NULL,
	[Login] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[FirstName] [varchar](50) NULL,
	[LastName] [varchar](50) NULL,
	[IdJob] [int] NULL,
	[IdDepartment] [int] NULL,
	[IdManager] [int] NULL,
	[CreateDate] [datetime] NULL,
	[Email] [varchar](100) NULL,
	[IsActive] [bit] NOT NULL,
	[ChangeDate] [datetime] NULL,
	[IdCompany] [int] NULL,
	[SimpleID] [int] NULL,
	[ChangePasswordDate] [datetime] NULL,
	[windkorp] [varchar](50) NULL,
	[nrTel] [varchar](50) NULL,
	[SLT] [int] NULL,
	[IsTest] [bit] NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[IdUser] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Role]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[IdRole] [int] IDENTITY(1,1) NOT NULL,
	[IdApplication] [int] NULL,
	[Name] [varchar](2000) NULL,
	[CreateDate] [datetime] NULL
) ON [PRIMARY]
SET ANSI_PADDING OFF
ALTER TABLE [dbo].[Role] ADD [Description] [varchar](max) NULL
ALTER TABLE [dbo].[Role] ADD  CONSTRAINT [PK_UprawnieniaTypy] PRIMARY KEY CLUSTERED 
(
	[IdRole] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[IdUserRole] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NULL,
	[IdRole] [int] NULL,
	[FromDate] [datetime] NULL,
 CONSTRAINT [PK_UprawnieniaUzytkownika] PRIMARY KEY CLUSTERED 
(
	[IdUserRole] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsersBranches]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsersBranches](
	[IdUser] [int] NOT NULL,
	[IdBranch] [int] NOT NULL,
	[IdJob] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_UsersBranches] PRIMARY KEY CLUSTERED 
(
	[IdUser] ASC,
	[IdBranch] ASC,
	[IdJob] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserData]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserData](
	[IdUser] [int] NOT NULL,
	[Footer] [varchar](max) NOT NULL,
 CONSTRAINT [PK_UserData] PRIMARY KEY CLUSTERED 
(
	[IdUser] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ApplicationDomain]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationDomain](
	[IdApplicationDomain] [int] IDENTITY(1,1) NOT NULL,
	[IdApplication] [int] NULL,
	[IdCompany] [int] NULL,
 CONSTRAINT [PK_DomenyAplikacji] PRIMARY KEY CLUSTERED 
(
	[IdApplicationDomain] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[JobRole]    Script Date: 02/27/2014 07:00:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[JobRole](
	[IdJobRole] [int] IDENTITY(1,1) NOT NULL,
	[IdJob] [int] NULL,
	[IdRole] [int] NULL,
	[FromDate] [datetime] NULL,
 CONSTRAINT [PK_UprawnieniaStanowiska] PRIMARY KEY CLUSTERED 
(
	[IdJobRole] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[AktualnaStruktura]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[AktualnaStruktura]
AS
SELECT     CurrentStructure AS aktualnaStruktura, IdCompany AS Firma
FROM         dbo.CurrentStructure
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CurrentStructure"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 95
               Right = 209
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AktualnaStruktura'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'AktualnaStruktura'
GO
/****** Object:  StoredProcedure [dbo].[p_DodajAktualnaStrukture]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_DodajAktualnaStrukture] 
@struktura XML,
@Firma INT

AS

DECLARE @ile AS INT

SET @ile = (SELECT COUNT(aktualnaStruktura) FROM AktualnaStruktura WHERE Firma = @Firma)

IF @ile>0
	BEGIN
		UPDATE AktualnaStruktura SET aktualnaStruktura = @struktura WHERE Firma = @Firma
	END
ELSE
	BEGIN
		INSERT INTO AktualnaStruktura VALUES(@struktura, @Firma) 
	END
GO
/****** Object:  StoredProcedure [dbo].[p_UsunStrukture]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_UsunStrukture]
AS

DELETE FROM AktualnaStruktura
DELETE FROM Jednostki
DELETE FROM Oddzialy
DELETE FROM Struktura
DELETE FROM Stanowiska
DELETE FROM Users
DELETE FROM ZmianyWStrukturze
GO
/****** Object:  StoredProcedure [dbo].[p_DodajZmianyStruktury]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_DodajZmianyStruktury] 
@zmiany XML, 
@Firma INT,
@NazwaPliku VARCHAR(50)

AS

INSERT INTO ZmianyWStrukturze (Dane, DATA, Firma, NazwaPliku) VALUES (@zmiany, GETDATE(), @Firma, @NazwaPliku)
GO
/****** Object:  StoredProcedure [dbo].[p_UsunJednostke]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_UsunJednostke]
@Id INT,
@Rodzaj VARCHAR(50),
@Firma INT

AS

    IF @Firma = 1 
        BEGIN
            SET @ID = @ID + 111000000
        END
    ELSE 
        BEGIN
            SET @ID = @ID + 222000000
        END

IF @Rodzaj = 'Jednostka'
BEGIN
	UPDATE Jednostki SET Aktywny = 0, DataOstatniejZmiany = GETDATE() WHERE ID = @Id
END

IF @Rodzaj= 'Oddział'
BEGIN
	UPDATE Oddzialy SET Aktywny = 0, DataOstatniejZmiany = GETDATE() WHERE Id = @Id 
END

IF @Rodzaj = 'Region'
BEGIN
	UPDATE Regiony SET Aktywny = 0, DataOstatniejZmiany = GETDATE() WHERE Id = @Id
END
GO
/****** Object:  StoredProcedure [dbo].[p_ZmienNazweJednostki]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_ZmienNazweJednostki]
    @RCPID INT,
    @NazwaPrzedZmiana VARCHAR(500),
    @NazwaPoZmianie VARCHAR(500),
    @RodzajPrzedZmiana VARCHAR(50),
    @RodzajPoZmianie VARCHAR(50),
    @NumerPoZmianie INT,
    @Firma INT
AS 
    DECLARE @IdStanowiska AS INT

    IF @Firma = 1 
        BEGIN
            SET @RCPID = @RCPID + 111000000
        END
    ELSE 
        BEGIN
            SET @RCPID = @RCPID + 222000000
        END

    IF @RodzajPrzedZmiana = 'Jednostka' 
        BEGIN
            UPDATE  Jednostki
            SET     Nazwa = @NazwaPoZmianie,
                    DataOstatniejZmiany = GETDATE()
            WHERE   ID = @RCPID 
        END

    IF @RodzajPrzedZmiana = 'Stanowisko' 
        BEGIN
            SELECT  Nazwa
            FROM    Stanowiska
            WHERE   Nazwa = @NazwaPoZmianie
                    AND Firma = @Firma
            IF @@ROWCOUNT = 0 
                BEGIN
                    SET @IdStanowiska = ( SELECT TOP 1
                                                    Id
                                          FROM      dbo.Stanowiska
                                          WHERE     Firma = @Firma
                                          ORDER BY  Id DESC
                                        ) + 1
                    INSERT  INTO Stanowiska
                            (
                              Id,
                              Nazwa,
                              DataUtworzenia,
                              Firma
                            )
                    VALUES  (
                              @IdStanowiska,
                              @NazwaPoZmianie,
                              GETDATE(),
                              @Firma
                            )
                END
        END
    IF @RodzajPrzedZmiana = 'Oddział' 
        BEGIN
            UPDATE  Oddzialy
            SET     Nazwa = @NazwaPoZmianie,
                    Numer = @NumerPoZmianie,
                    DataOstatniejZmiany = GETDATE()
            WHERE   Id = @RCPID 
        END

    IF @RodzajPrzedZmiana = 'Region' 
        BEGIN
            UPDATE  Regiony
            SET     Nazwa = @NazwaPoZmianie,
                    DataOstatniejZmiany = GETDATE()
            WHERE   Id = @RCPID 
        END
GO
/****** Object:  StoredProcedure [dbo].[p_DodajJednostkeOrganizacyjna]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_DodajJednostkeOrganizacyjna]
    @RCPID INT,
    @Nazwa VARCHAR(200),
    @NumerOddzialu INT,
    @Rodzaj VARCHAR(50),
    @Firma INT
AS 
    DECLARE @IdStanowiska AS INT

    IF @Firma = 1 
        BEGIN
            SET @RCPID = @RCPID + 111000000
        END
    ELSE 
        BEGIN
            SET @RCPID = @RCPID + 222000000
        END

    IF @Rodzaj = 'Jednostka' 
        BEGIN
            SELECT  Nazwa
            FROM    Jednostki
            WHERE   Nazwa = @Nazwa
                    AND Firma = @Firma
            IF @@ROWCOUNT = 0 
                BEGIN
                    INSERT  INTO Jednostki
                            (
                              Nazwa,
                              ID,
                              DataUtworzenia,
                              Aktywny,
                              dbo.Firma
                            )
                    VALUES  (
                              @nazwa,
                              @RCPID,
                              GETDATE(),
                              1,
                              @Firma
                            ) 
                END
        END
    IF @Rodzaj = 'Stanowisko' 
        BEGIN
            SELECT  Nazwa
            FROM    Stanowiska
            WHERE   Nazwa = @Nazwa
                    AND Firma = @Firma
            IF @@ROWCOUNT = 0 
                BEGIN
                    SET @IdStanowiska = ( SELECT TOP 1
                                                    Id
                                          FROM      dbo.Stanowiska
                                          WHERE     Firma = @Firma
                                          ORDER BY  Id DESC
                                        ) + 1
                    INSERT  INTO Stanowiska
                            (
                              Id,
                              Nazwa,
                              DataUtworzenia,
                              Aktywny,
                              dbo.Firma
                            )
                    VALUES  (
                              @IdStanowiska,
                              @nazwa,
                              GETDATE(),
                              1,
                              @Firma
                            )
                END
        END
    IF @Rodzaj = 'Oddział' 
        BEGIN
            SELECT  Nazwa
            FROM    Oddzialy
            WHERE   Nazwa = @Nazwa
                    AND Firma = @Firma
            IF @@ROWCOUNT = 0 
                BEGIN
                    INSERT  INTO Oddzialy
                            (
                              Nazwa,
                              Numer,
                              ID,
                              DataUtworzenia,
                              Aktywny,
                              dbo.Firma
                            )
                    VALUES  (
                              @nazwa,
                              @NumerOddzialu,
                              @RCPID,
                              GETDATE(),
                              1,
                              @Firma
                            )
                END
        END

    IF @Rodzaj = 'Region' 
        BEGIN
            SELECT  Nazwa
            FROM    Regiony
            WHERE   Nazwa = @Nazwa
                    AND Firma = @Firma
            IF @@ROWCOUNT = 0 
                BEGIN
                    INSERT  INTO Regiony
                            (
                              Nazwa,
                              ID,
                              DataUtworzenia,
                              Aktywny,
                              dbo.Firma
                            )
                    VALUES  (
                              @nazwa,
                              @RCPID,
                              GETDATE(),
                              1,
                              @Firma
                            )
                END
        END
GO
/****** Object:  StoredProcedure [dbo].[p_DodajUsera]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_DodajUsera]
    @ID INT,
    @Imie VARCHAR(500),
    @Nazwisko VARCHAR(500),
    @Stanowisko VARCHAR(500),
    @Oddzial VARCHAR(500),
    @Jednostka VARCHAR(500),
    @Region VARCHAR(500),
    @Firma INT
AS 
    DECLARE @IdOddzialu AS INT,
        @IdStanowiska AS INT,
        @IdJednostki AS INT,
        @IdRegionu AS INT

    IF @Firma = 1 
        BEGIN
            SET @ID = @ID + 111000000
        END
    ELSE 
        BEGIN
            SET @ID = @ID + 222000000
        END
    SET @IdStanowiska = ( SELECT    Id
                          FROM      Stanowiska
                          WHERE     Nazwa = @Stanowisko
                                    AND Firma = @Firma
                        )
    SET @IdOddzialu = ( SELECT  Id
                        FROM    Oddzialy
                        WHERE   Nazwa = @Oddzial
                                AND Firma = @Firma
                      )
    SET @IdJednostki = ( SELECT Id
                         FROM   Jednostki
                         WHERE  Nazwa = @Jednostka
                                AND Firma = @Firma
                       )
    SET @IdRegionu = ( SELECT   Id
                       FROM     Regiony
                       WHERE    Nazwa = @Region
                                AND Firma = @Firma
                     )

    INSERT  INTO Users
            (
              ID,
              Imie,
              Nazwisko,
              stanowisko,
              oddzial,
              dzial,
              region,
              DataUtworzenia,
              aktywny,
              dbo.Firma
            )
    VALUES  (
              @ID,
              @Imie,
              @Nazwisko,
              @IdStanowiska,
              @IdOddzialu,
              @IdJednostki,
              @IdRegionu,
              GetDate(),
              1,
              @Firma
            ) -- int 2 odpowiada za przypisanie pracownika ze struktury automatycznie do SKOK Stefczyka
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderGetUserProfile]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderGetUserProfile] @IdUser INT, @IdBranch INT = NULL 

AS

IF @IdBranch IS NULL
BEGIN
	SELECT 
		u.IdUser AS Id,
		u.FirstName AS Imie, 
		u.LastName AS Nazwisko, 
		ISNULL(j.Name, '') AS Stanowisko,
		ISNULL(d.Name, '') AS Dzial,
		ISNULL(b.Name, '') AS Oddzial,
		ISNULL(b.NoBranch, '') AS NrOddzialu,
		ISNULL(b.NoSKOK, '') AS NrSKOK,
		ISNULL(b.IdRegion, 0) AS IdRegion,
		ISNULL(u.IdManager, '') AS IdKierownika,
		ISNULL(u1.LastName, '') AS NazwiskoKierownika,
		ISNULL(u1.FirstName, '') AS ImieKierownika, 
		ISNULL(r.Name, 'Centrala') AS Region,
		ISNULL(u.IdJob, 0) AS IdStanowisko,
		ISNULL(b.IdBranch, 0) AS IdOddzial,
		ISNULL(u.IdCompany, 0) AS IdFirma,
		ISNULL(c.Name, '') AS Firma,
		ISNULL(c.Adress, '') AS FirmaAdres,
		ISNULL(c.Suffix, '') AS Suffix,
		ISNULL(u.IdDepartment, 0) AS IdDzial,
		ISNULL(u.Email, '') as Email,
		ISNULL(u.nrTel, '') as nrTel,
		ISNULL(u.SimpleID, 0) AS SimpleID,
		ISNULL(ud.Footer, '') AS Footer,
		STUFF((SELECT tr.Name + ',' FROM Role tr LEFT JOIN UserRole tur ON tr.IdRole = tur.IdRole WHERE tur.IdUser = u.IdUser FOR XML PATH('')), 1, 0, '') AS Roles
	FROM dbo.[User] u
		LEFT JOIN dbo.Job j ON u.IdJob = j.IdJob
		LEFT JOIN dbo.Department d ON u.IdDepartment = d.IdDepartment
		LEFT JOIN dbo.UsersBranches ub ON u.IdUser = ub.IdUser 
		LEFT JOIN dbo.Branch b ON ub.IdBranch = b.IdBranch
		LEFT JOIN dbo.[User] u1 ON u.IdManager = u1.IdUser
		LEFT JOIN dbo.Region r ON b.IdRegion = r.IdRegion
		LEFT JOIN dbo.Company c ON u.IdCompany = c.IdCompany
		LEFT JOIN dbo.UserData ud ON u.IdUser = ud.IdUser
	WHERE u.IdUser = @IdUser AND u.IsActive = 1	 
END
ELSE 
BEGIN
	SELECT 
		u.IdUser AS Id,
		u.FirstName AS Imie, 
		u.LastName AS Nazwisko, 
		ISNULL(j.Name, '') AS Stanowisko,
		ISNULL(d.Name, '') AS Dzial,
		ISNULL(b.Name, '') AS Oddzial,
		ISNULL(b.NoBranch, '') AS NrOddzialu,
		ISNULL(b.NoSKOK, '') AS NrSKOK,
		ISNULL(b.IdRegion, 0) AS IdRegion,
		ISNULL(u.IdManager, '') AS IdKierownika,
		ISNULL(u1.LastName, '') AS NazwiskoKierownika,
		ISNULL(u1.FirstName, '') AS ImieKierownika, 
		ISNULL(r.Name, 'Centrala') AS Region,
		ISNULL(j.IdJob, 0) AS IdStanowisko,
		ISNULL(b.IdBranch, 0) AS IdOddzial,
		ISNULL(u.IdCompany, 0) AS IdFirma,
		ISNULL(c.Name, '') AS Firma,
		ISNULL(c.Adress, '') AS FirmaAdres,
		ISNULL(c.Suffix, '') AS Suffix,
		ISNULL(u.IdDepartment, 0) AS IdDzial,
		ISNULL(u.Email, '') as Email,
		ISNULL(u.nrTel, '') as nrTel,
		ISNULL(u.SimpleID, 0) AS SimpleID,
		ISNULL(ud.Footer, '') AS Footer,
		STUFF((SELECT tr.Name + ',' FROM Role tr LEFT JOIN UserRole tur ON tr.IdRole = tur.IdRole WHERE tur.IdUser = u.IdUser FOR XML PATH('')), 1, 0, '') AS Roles
	FROM dbo.[User] u
		LEFT JOIN dbo.Department d ON u.IdDepartment = d.IdDepartment
		LEFT JOIN dbo.UsersBranches ub ON u.IdUser = ub.IdUser 
		LEFT JOIN dbo.Branch b ON ub.IdBranch = b.IdBranch
		LEFT JOIN dbo.Job j ON ub.IdJob = j.IdJob
		LEFT JOIN dbo.[User] u1 ON u.IdManager = u1.IdUser
		LEFT JOIN dbo.Region r ON b.IdRegion = r.IdRegion
		LEFT JOIN dbo.Company c ON u.IdCompany = c.IdCompany
		LEFT JOIN dbo.UserData ud ON u.IdUser = ud.IdUser
	WHERE u.IdUser = @IdUser AND u.IsActive = 1 AND ub.IdBranch = @IdBranch
END
GO
/****** Object:  View [dbo].[Regiony]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Regiony]
AS
SELECT     IdRegion AS ID, Name AS Nazwa, CreateDate AS DataUtworzenia, ChangeDate AS DataOstatniejZmiany, isActive AS Aktywny, IdCompany AS Firma
FROM         dbo.Region
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Region"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 291
               Right = 356
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Regiony'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Regiony'
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_CreateRegion]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_CreateRegion] 
    @Name VARCHAR(500),
    @IdCompany INT
AS 

 DECLARE @IdRegion AS INT
 
 SET @IdRegion = (SELECT MAX(IdRegion)+1 FROM dbo.Region WHERE IdCompany = @IdCompany)
 
 IF @IdRegion IS NULL
 BEGIN
	SET @IdRegion = ((SELECT Preffix FROM dbo.Company WHERE IdCompany = @IdCompany) * 1000000 )+ 1
 END
 
 INSERT INTO [dbo].Region
        ( IdRegion ,
          Name ,
          CreateDate ,
          IsActive ,
          IdCompany
        )
VALUES  ( @IdRegion
          ,@Name
          ,GETDATE()
          ,1
          ,@IdCompany
        )
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_UpdateRegion]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_UpdateRegion] 
	@IdRegion INT,
    @Name VARCHAR(500),
	@IsActive BIT
    
AS 

UPDATE [dbo].[Region]
   SET [Name] = @Name
      ,[ChangeDate] = GETDATE()
      ,[IsActive] = @IsActive
 WHERE [IdRegion] = @IdRegion
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_CreateRegionFromStructure]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_CreateRegionFromStructure] 
    @IdRegion INT,
    @Name VARCHAR(500),
    @IdCompany INT
AS 

 
 INSERT INTO [dbo].Region
        ( IdRegion ,
          Name ,
          CreateDate ,
          IsActive ,
          IdCompany
        )
VALUES  ( @IdRegion
          ,@Name
          ,GETDATE()
          ,1
          ,@IdCompany
        )
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderGetUsersProfiles]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderGetUsersProfiles] @IdCompany INT = NULL 

AS

IF @IdCompany IS NULL
BEGIN
	SELECT 
		u.IdUser AS Id,
		u.FirstName AS Imie, 
		u.LastName AS Nazwisko, 
		ISNULL(j.Name, '') AS Stanowisko,
		ISNULL(d.Name, '') AS Dzial,
		ISNULL(b.Name, '') AS Oddzial,
		ISNULL(b.NoBranch, '') AS NrOddzialu,
		ISNULL(b.NoSKOK, '') AS NrSKOK,
		ISNULL(b.IdRegion, 0) AS IdRegion,
		ISNULL(u.IdManager, '') AS IdKierownika,
		ISNULL(u1.LastName, '') AS NazwiskoKierownika,
		ISNULL(u1.FirstName, '') AS ImieKierownika, 
		ISNULL(r.Name, 'Centrala') AS Region,
		ISNULL(u.IdJob, 0) AS IdStanowisko,
		ISNULL(b.IdBranch, 0) AS IdOddzial,
		ISNULL(u.IdCompany, 0) AS IdFirma,
		ISNULL(c.Name, '') AS Firma,
		ISNULL(c.Adress, '') AS FirmaAdres,
		ISNULL(c.Suffix, '') AS Suffix,
		ISNULL(u.IdDepartment, 0) AS IdDzial,
		ISNULL(u.Email, '') as Email,
		ISNULL(u.nrTel, '') as nrTel,
		ISNULL(ud.Footer, '') AS Footer,
		STUFF((SELECT tr.Name + ',' FROM Role tr LEFT JOIN UserRole tur ON tr.IdRole = tur.IdRole WHERE tur.IdUser = u.IdUser FOR XML PATH('')), 1, 0, '') AS Roles
	FROM dbo.[User] u
		LEFT JOIN dbo.Job j ON u.IdJob = j.IdJob
		LEFT JOIN dbo.Department d ON u.IdDepartment = d.IdDepartment
		LEFT JOIN dbo.UsersBranches ub ON u.IdUser = ub.IdUser 
		LEFT JOIN dbo.Branch b ON ub.IdBranch = b.IdBranch
		LEFT JOIN dbo.[User] u1 ON u.IdManager = u1.IdUser
		LEFT JOIN dbo.Region r ON b.IdRegion = r.IdRegion
		LEFT JOIN dbo.Company c ON u.IdCompany = c.IdCompany
		LEFT JOIN dbo.UserData ud ON u.IdUser = ud.IdUser
	WHERE u.IsActive = 1	 
END
ELSE 
BEGIN
	SELECT 
		u.IdUser AS Id,
		u.FirstName AS Imie, 
		u.LastName AS Nazwisko, 
		ISNULL(j.Name, '') AS Stanowisko,
		ISNULL(d.Name, '') AS Dzial,
		ISNULL(b.Name, '') AS Oddzial,
		ISNULL(b.NoBranch, '') AS NrOddzialu,
		ISNULL(b.NoSKOK, '') AS NrSKOK,
		ISNULL(b.IdRegion, 0) AS IdRegion,
		ISNULL(u.IdManager, '') AS IdKierownika,
		ISNULL(u1.LastName, '') AS NazwiskoKierownika,
		ISNULL(u1.FirstName, '') AS ImieKierownika, 
		ISNULL(r.Name, 'Centrala') AS Region,
		ISNULL(u.IdJob, 0) AS IdStanowisko,
		ISNULL(b.IdBranch, 0) AS IdOddzial,
		ISNULL(u.IdCompany, 0) AS IdFirma,
		ISNULL(c.Name, '') AS Firma,
		ISNULL(c.Adress, '') AS FirmaAdres,
		ISNULL(c.Suffix, '') AS Suffix,
		ISNULL(u.IdDepartment, 0) AS IdDzial,
		ISNULL(u.Email, '') as Email,
		ISNULL(u.nrTel, '') as nrTel,
		ISNULL(ud.Footer, '') AS Footer,
		STUFF((SELECT tr.Name + ',' FROM Role tr LEFT JOIN UserRole tur ON tr.IdRole = tur.IdRole WHERE tur.IdUser = u.IdUser FOR XML PATH('')), 1, 0, '') AS Roles
	FROM dbo.[User] u
		LEFT JOIN dbo.Job j ON u.IdJob = j.IdJob
		LEFT JOIN dbo.Department d ON u.IdDepartment = d.IdDepartment
		LEFT JOIN dbo.UsersBranches ub ON u.IdUser = ub.IdUser 
		LEFT JOIN dbo.Branch b ON ub.IdBranch = b.IdBranch
		LEFT JOIN dbo.[User] u1 ON u.IdManager = u1.IdUser
		LEFT JOIN dbo.Region r ON b.IdRegion = r.IdRegion
		LEFT JOIN dbo.Company c ON u.IdCompany = c.IdCompany
		LEFT JOIN dbo.UserData ud ON u.IdUser = ud.IdUser
	WHERE u.IsActive = 1 AND u.IdCompany = @IdCompany
END
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_UpdateDepartment]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_UpdateDepartment] 
	@IdDepartment INT,
    @Name VARCHAR(500),
	@IsActive BIT
    
AS 

UPDATE [dbo].[Department]
   SET [Name] = @Name
      ,[ChangeDate] = GETDATE()
      ,[IsActive] = @IsActive
 WHERE [IdDepartment] = @IdDepartment
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_CreateDepartmentFromStructure]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_CreateDepartmentFromStructure] 
    @IdDepartment INT,
    @Name VARCHAR(500),
    @IdCompany INT
AS 
 
 
 INSERT INTO [dbo].Department
        ( IdDepartment ,
          Name ,
          CreateDate ,
          IsActive ,
          IdCompany
        )
VALUES  ( @IdDepartment
          ,@Name
          ,GETDATE()
          ,1
          ,@IdCompany
        )
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_CreateDepartment]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_CreateDepartment] 
    @Name VARCHAR(500),
    @IdCompany INT
AS 

 DECLARE @IdDepartment AS INT
 
 SET @IdDepartment = (SELECT MAX(IdDepartment)+1 FROM dbo.Department WHERE IdCompany = @IdCompany)
 
 IF @IdDepartment IS NULL
 BEGIN
	SET @IdDepartment = ((SELECT Preffix FROM dbo.Company WHERE IdCompany = @IdCompany) * 1000000 )+ 1
 END
 
 
 INSERT INTO [dbo].Department
        ( IdDepartment ,
          Name ,
          CreateDate ,
          IsActive ,
          IdCompany
        )
VALUES  ( @IdDepartment
          ,@Name
          ,GETDATE()
          ,1
          ,@IdCompany
        )
GO
/****** Object:  View [dbo].[Jednostki]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Jednostki]
AS
SELECT     IdDepartment AS ID, Name AS Nazwa, CreateDate AS DataUtworzenia, ChangeDate AS DataOstatniejZmiany, isActive AS Aktywny, IdCompany AS Firma
FROM         dbo.Department
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Department"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 307
               Right = 358
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Jednostki'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Jednostki'
GO
/****** Object:  View [dbo].[Users]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Users]
AS
SELECT     IdUser AS ID, Login, Password AS haslo, FirstName AS Imie, LastName AS Nazwisko, IdJob AS Stanowisko, IdDepartment AS Dzial, IdManager AS Kierownik, 
                      CreateDate AS DataUtworzenia, Email, IsActive AS aktywny, ChangeDate AS DataOstatniejZmiany, IdCompany AS Firma, SimpleID, 
                      ChangePasswordDate AS DataOstatniejZmianyHasla, windkorp, nrTel, SLT
FROM         dbo.[User]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[32] 4[25] 2[26] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "User"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 282
               Right = 233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3180
         Alias = 2490
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Users'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Users'
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderGetUserManagers]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderGetUserManagers] @IdUser INT

AS


DECLARE @idKierownika AS INT
CREATE TABLE #tmp
(
Id INT,
Imie VARCHAR(50),
Nazwisko VARCHAR(200)
)

SET @idKierownika = (SELECT IdManager FROM dbo.[User] WHERE IdUser = @IdUser)

WHILE @idKierownika IS NOT NULL
BEGIN
	INSERT INTO #tmp (Id, Imie, Nazwisko) (SELECT IdUser, FirstName, LastName FROM dbo.[User] WHERE IdUser = @IdKierownika)
	SET @idKierownika = (SELECT IdManager FROM dbo.[User] WHERE IdUser = @idKierownika)
END


SELECT * FROM #tmp
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderIsManager]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderIsManager] @IdUser INT

AS

SELECT COUNT(*) AS CzyKierownik FROM dbo.[User] WHERE IdManager = @IdUser
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderGetDependetNoSKOK]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderGetDependetNoSKOK] @IdUser INT



AS



--DECLARE @idCompany AS INT



--SET @idCompany = (SELECT IdCompany FROM dbo.[User] WHERE IdUser = @IdUser)

--IF @idCompany = 14
--BEGIN 
--	SELECT IdCompany, Name, NoSKOK FROM dbo.Company WHERE IdCompany = 1
--END
--ELSE
--BEGIN 
--	SELECT IdCompany, Name, NoSKOK  FROM dbo.Company WHERE IdCompany = @idCompany
--END


WITH Tree(IdUser, UserLevel) AS 
(
    SELECT IdUser, 0 AS UserLevel
    FROM dbo.[User] 
    WHERE IsActive = 1 and IdManager = @IdUser
    UNION ALL
    SELECT u.IdUser, UserLevel + 1
    FROM dbo.[User] AS u
        INNER JOIN Tree AS t
        ON u.IdManager = t.IdUser 
        WHERE u.IsActive = 1
)
SELECT c.IdCompany, c.Name, b.NoSKOK --IdUser, UserLevel 
FROM Tree t INNER JOIN dbo.UsersBranches ub ON t.IdUser = ub.IdUser
INNER JOIN dbo.Branch b ON ub.IdBranch = b.IdBranch
INNER JOIN dbo.Company c ON b.NoSKOK = c.NoSKOK 
GROUP BY c.IdCompany, c.Name, b.NoSKOK

IF @@ROWCOUNT = 0
BEGIN
	SELECT c.IdCompany, c.Name, b.NoSKOK --IdUser, UserLevel 
	FROM dbo.[User] u INNER JOIN dbo.UsersBranches ub ON u.IdUser = ub.IdUser
	INNER JOIN dbo.Branch b ON ub.IdBranch = b.IdBranch
	INNER JOIN dbo.Company c ON b.NoSKOK = c.NoSKOK 
	WHERE u.IdUser = @IdUser
	GROUP BY c.IdCompany, c.Name, b.NoSKOK
END 


--create table #tree (id INT , levelNo int)
--insert #tree select u.IdUser, 1 from dbo.[User] u where u.IdManager = @IdUser

--IF @@ROWCOUNT = 0
--BEGIN
--	insert #tree select u.IdUser, 1 from dbo.[User] u where u.IdUser = @IdUser
--END 

--declare @i int
--select @i = 0
--while @@rowcount > 0
--begin
--	 select @i = @i + 1 
	 
--     insert #tree
--     -- Get all children of previous level
--     select u.IdUser, @i + 1
--     from dbo.[User] u, #tree
--     where #tree.levelNo = @i
--     and u.IdManager = #tree.id AND u.IsActive = 1
--end

--SELECT DISTINCT c.IdCompany, c.Name, b.NoSKOK  
--FROM #tree, dbo.UsersBranches ub, dbo.Branch b, dbo.Company c
--WHERE #tree.id = ub.IdUser AND ub.IdBranch = b.IdBranch AND b.NoSKOK = c.NoSKOK

--DROP TABLE #tree
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderGetJobRoles]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderGetJobRoles] @IdUser INT, @ApplicationName VARCHAR(500), @IdBranch INT = NULL 

AS

IF @IdBranch IS NULL
BEGIN
	SELECT DISTINCT r.Name FROM dbo.Role r
					LEFT JOIN dbo.Application a ON r.IdApplication = a.IdApplication
					LEFT JOIN dbo.JobRole jr ON r.IdRole = jr.IdRole
					LEFT JOIN dbo.[User] u ON jr.IdJob = u.IdJob
				  WHERE u.IdUser = @IdUser AND a.Name = @ApplicationName		 
END
ELSE 
BEGIN
	SELECT DISTINCT r.Name FROM dbo.Role r
					LEFT JOIN dbo.Application a ON r.IdApplication = a.IdApplication
					LEFT JOIN dbo.JobRole jr ON r.IdRole = jr.IdRole
					LEFT JOIN dbo.[UsersBranches] ub ON jr.IdJob = ub.IdJob
				  WHERE ub.IdUser = @IdUser AND a.Name = @ApplicationName AND ub.IdBranch = @IdBranch
END
GO
/****** Object:  StoredProcedure [dbo].[p_pobierzOdzialyDlaRegionalnego]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_pobierzOdzialyDlaRegionalnego] @id INT

AS


WITH Tree(IdUser, UserLevel) AS 
(
    SELECT IdUser, 0 AS UserLevel
    FROM dbo.[User] 
    WHERE IdManager = @id AND IsActive = 1
    UNION ALL
    SELECT u.IdUser, UserLevel + 1
    FROM dbo.[User] AS u
        INNER JOIN Tree AS t
        ON u.IdManager = t.IdUser 
)
SELECT b.NoBranch --IdUser, UserLevel 
FROM Tree t INNER JOIN dbo.UsersBranches ub ON t.IdUser = ub.IdUser
INNER JOIN dbo.Branch b ON ub.IdBranch = b.IdBranch WHERE b.NoSKOK = 65
GROUP BY b.NoBranch
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_UpdateUser]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_UpdateUser] 
	@IdUser INT,
    @FirstName VARCHAR(500),
    @LastName VARCHAR(500),
    @IdJob INT,
    @IdDepartment INT,
    @IdManager INT,
    @Email VARCHAR(100),
    @IsActive BIT,
    @SimpleID INT
AS 

UPDATE [dbo].[User]
   SET [FirstName] = @FirstName
      ,[LastName] = @LastName
      ,[IdJob] = @IdJob
      ,[IdDepartment] = @IdDepartment
      ,[IdManager] = @IdManager
      ,[Email] = @Email
      ,[IsActive] = @IsActive
      ,[ChangeDate] = GETDATE()
      ,[SimpleID] = @SimpleID
 WHERE IdUser = @IdUser
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_CreateUserFromStructure]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_CreateUserFromStructure] 
    @IdUser INT,
    @FirstName VARCHAR(500),
    @LastName VARCHAR(500),
    @IdJob INT,
    @IdDepartment INT,
    @IdManager INT,
    @Email VARCHAR(100),
    @IsActive BIT,
    @IdCompany INT,
    @SimpleID INT
AS 
 
 INSERT INTO [dbo].[User]
           ([IdUser]
           ,[FirstName]
           ,[LastName]
           ,[IdJob]
           ,[IdDepartment]
           ,[IdManager]
           ,[CreateDate]
           ,[Email]
           ,[IsActive]
           ,[IdCompany]
           ,[IsTest]
           ,[SimpleID])
     VALUES
           (@IdUser
           ,@FirstName
           ,@LastName
           ,@IdJob
           ,@IdDepartment
           ,@IdManager
           ,GETDATE()
           ,@Email
           ,@IsActive
           ,@IdCompany
           ,0
           ,@SimpleID)
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_CreateUser]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_CreateUser] 
    @FirstName VARCHAR(500),
    @LastName VARCHAR(500),
    @IdJob INT,
    @IdDepartment INT,
    @IdManager INT,
    @Email VARCHAR(100),
    @IsActive BIT,
    @IdCompany INT
AS 

 DECLARE @IdUser AS INT
 
 SET @IdUser = (SELECT MAX([IdUser])+1 FROM [dbo].[User] WHERE [IdCompany] = @IdCompany)
 
 IF @IdUser IS NULL 
   BEGIN
	SET @IdUser = ((SELECT Preffix FROM dbo.Company WHERE IdCompany = @IdCompany) * 1000000 )+ 1
 END
 
 INSERT INTO [dbo].[User]
           ([IdUser]
           ,[FirstName]
           ,[LastName]
           ,[IdJob]
           ,[IdDepartment]
           ,[IdManager]
           ,[CreateDate]
           ,[Email]
           ,[IsActive]
           ,[IdCompany],
           [IsTest])
     VALUES
           (@IdUser
           ,@FirstName
           ,@LastName
           ,@IdJob
           ,@IdDepartment
           ,@IdManager
           ,GETDATE()
           ,@Email
           ,@IsActive
           ,@IdCompany
           ,0)

SELECT @IdUser AS IdUser
GO
/****** Object:  StoredProcedure [dbo].[p_PobierzListePracownikowKPKW]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_PobierzListePracownikowKPKW] 
@IdUser INT,
@IdBranch INT,
@Type INT

AS

IF @Type = 1
BEGIN
	SELECT 
	IdUser AS ID,
	LastName + ' ' + FirstName AS 'ImieNazwisko'
	FROM [Uzytkownicy].[dbo].[User] 
	WHERE IdManager = @IdUser AND IsActive = 1 ORDER BY LastName ASC 
END

IF @Type = 2
BEGIN
 SELECT
 u.IdUser as ID,
 u.LastName + ' ' + u.FirstName AS 'ImieNazwisko'
 FROM [Uzytkownicy].[dbo].[UsersBranches] b 
 LEFT JOIN [Uzytkownicy].[dbo].[User] u ON b.IdUser = u.IdUser
 WHERE b.IdBranch = @IdBranch AND u.IsActive = 1 AND u.IdJob != 111020053 ORDER BY u.LastName ASC
END
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderGetAllRoles]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderGetAllRoles] @ApplicationName VARCHAR(500)

AS

SELECT DISTINCT r.Name, r.Description FROM dbo.Role r
				LEFT JOIN dbo.Application a ON r.IdApplication = a.IdApplication
			  WHERE a.Name = @ApplicationName
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderAddUserToRole]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderAddUserToRole] @IdUser INT, @rolename VARCHAR(2000)

AS

DECLARE @chk INT
DECLARE @IdRole INT

SELECT @IdRole = r.IdRole, @chk = COUNT(ur.IdUser) FROM Role r
LEFT JOIN UserRole ur ON r.IdRole = ur.IdRole AND ur.IdUser = 666000013
WHERE r.Name = @rolename
GROUP BY r.IdRole	 

IF @chk = 0
	INSERT INTO [dbo].[UserRole]
           ([IdUser]
           ,[IdRole]
           ,[FromDate])
     VALUES
           (@IdUser
           ,@IdRole
           ,GETDATE())
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderRemoveUserToRole]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderRemoveUserToRole] @IdUser INT, @rolename VARCHAR(2000)

AS

DECLARE @chk INT
DECLARE @IdRole INT

SELECT @IdRole = r.IdRole, @chk = COUNT(ur.IdUser) FROM Role r
LEFT JOIN UserRole ur ON r.IdRole = ur.IdRole AND ur.IdUser = 666000013
WHERE r.Name = @rolename
GROUP BY r.IdRole	 

IF @chk > 0
	DELETE FROM [dbo].[UserRole]
      WHERE IdRole = @IdRole AND IdUser = @IdUser
GO
/****** Object:  View [dbo].[Uprawnienia]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Uprawnienia]
AS
SELECT     IdRole AS IdUprawnienia, IdApplication AS IdAplikacji, Name AS Nazwa, CreateDate AS DataDodania
FROM         dbo.Role
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Role"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 288
               Right = 317
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1155
         Alias = 2250
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Uprawnienia'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Uprawnienia'
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderGetUserRoles]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderGetUserRoles] @IdUser INT, @ApplicationName VARCHAR(500)

AS

SELECT DISTINCT r.Name FROM dbo.Role r
				LEFT JOIN dbo.Application a ON r.IdApplication = a.IdApplication
				LEFT JOIN dbo.UserRole ur ON r.IdRole = ur.IdRole
			  WHERE ur.IdUser = @IdUser AND a.Name = @ApplicationName
GO
/****** Object:  View [dbo].[UprawnieniaStanowiska]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UprawnieniaStanowiska]
AS
SELECT     IdJobRole AS Id, IdJob AS IdStanowiska, IdRole AS IdUprawnienia, FromDate AS DataOd
FROM         dbo.JobRole
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "JobRole"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 248
               Right = 331
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 1995
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'UprawnieniaStanowiska'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'UprawnieniaStanowiska'
GO
/****** Object:  View [dbo].[UprawnieniaUzytkownika]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UprawnieniaUzytkownika]
AS
SELECT     IdUserRole AS Id, IdUser, IdRole AS IdUprawnienia, FromDate AS DataOd
FROM         dbo.UserRole
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "UserRole"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 230
               Right = 342
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'UprawnieniaUzytkownika'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'UprawnieniaUzytkownika'
GO
/****** Object:  View [dbo].[UprawnieniaAplikacje]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[UprawnieniaAplikacje]
AS
SELECT     IdApplication AS IdAplikacji, Name AS Nazwa, Description AS Opis, CreateDate AS DataDodania
FROM         dbo.Application
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Application"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 248
               Right = 303
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'UprawnieniaAplikacje'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'UprawnieniaAplikacje'
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_CreateJob]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_CreateJob] 
    @Name VARCHAR(500),
    @IdCompany INT
AS 

 DECLARE @IdJob AS INT
 
 SET @IdJob = (SELECT MAX(IdJob)+1 FROM dbo.Job WHERE IdCompany = @IdCompany)
 
 IF @IdJob IS NULL
 BEGIN
	SET @IdJob = ((SELECT Preffix FROM dbo.Company WHERE IdCompany = @IdCompany) * 1000000 )+ 1
 END
 
 INSERT INTO [dbo].Job
        ( IdJob ,
          Name ,
          CreateDate ,
          IsActive ,
          IdCompany
        )
     VALUES
           (@IdJob
           ,@Name
           ,GETDATE()
           ,1
           ,@IdCompany)
GO
/****** Object:  View [dbo].[Stanowiska]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Stanowiska]
AS
SELECT     IdJob AS Id, Name AS Nazwa, CreateDate AS DataUtworzenia, ChangeDate AS DataOstatniejZmiany, IsActive AS Aktywny, IdCompany AS Firma
FROM         dbo.Job
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Job"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 2
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Stanowiska'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Stanowiska'
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_UpdateJob]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_UpdateJob] 
	@IdJob INT,
    @Name VARCHAR(500),
	@IsActive BIT
    
AS 

UPDATE [dbo].[Job]
   SET [Name] = @Name
      ,[ChangeDate] = GETDATE()
      ,[IsActive] = @IsActive
 WHERE [IdJob] = @IdJob
GO
/****** Object:  StoredProcedure [dbo].[p_ZmienUsera]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_ZmienUsera]
@PersonRCPID INT,
@Imie VARCHAR(50),
@Nazwisko VARCHAR(50),
@StanowiskoZmienione VARCHAR(500),
@Stanowisko VARCHAR(500),
@Oddzial VARCHAR(500),
@Firma INT

AS 

    IF @Firma = 1 
        BEGIN
            SET @PersonRCPID = @PersonRCPID + 111000000
        END
    ELSE 
        BEGIN
            SET @PersonRCPID = @PersonRCPID + 222000000
        END
        
IF @Imie != ''
BEGIN
	UPDATE Users SET Imie = @Imie WHERE Id= @PersonRCPID
END

IF @Nazwisko != ''
BEGIN
	UPDATE Users SET Nazwisko = @Nazwisko WHERE Id = @PersonRCPID
END

IF @Stanowisko != '' OR @StanowiskoZmienione != ''
BEGIN
	UPDATE Users SET stanowisko = (SELECT Id FROM Stanowiska WHERE Nazwa = @Stanowisko AND Firma = @Firma AND Aktywny = 1) WHERE Id = @PersonRCPID	
END

IF @Oddzial != ''
BEGIN
	UPDATE Users SET oddzial = (SELECT Id FROM Oddzialy WHERE Nazwa = @Oddzial  AND Firma = @Firma AND Aktywny = 1) WHERE Id = @PersonRCPID
END

	UPDATE Users SET DataOstatniejZmiany = GETDATE() WHERE Id = @PersonRCPID
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderGetUserBranches]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderGetUserBranches] @IdUser INT
AS

SELECT b.IdBranch,
		b.Name,
		b.NoBranch,
		b.NoSKOK
FROM dbo.UsersBranches ub LEFT JOIN dbo.Branch b 
ON ub.IdBranch = b.IdBranch
WHERE ub.IdUser = @IdUser
GO
/****** Object:  StoredProcedure [dbo].[p_LogowanieWindKorp2]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_LogowanieWindKorp2]

@login VARCHAR(50)=null,
@haslo VARCHAR(50)=null,
@haslo_nowe VARCHAR(50)=NULL,
@imie VARCHAR(50)=NULL,
@nazwisko VARCHAR(50)=NULL,
@grupa VARCHAR(50)=NULL,
@uzytkownik VARCHAR(50)=NULL,
@aktywny BIT=1,
@id INT=NULL,
@opcja int

AS
BEGIN

DECLARE @id_max INT

--logowanie
IF @opcja=1
	SELECT windkorp AS dostep,ISNULL(Imie,'') + ' ' + isnull(Nazwisko,'') AS imieNazwisko,ISNULL(DataOstatniejZmianyHasla,'') AS DataOstatniejZmianyHasla,aktywny FROM dbo.Users 
	WHERE [login]=@login AND haslo=@haslo AND aktywny=1 AND id between 777000000 AND 778000000 
	
	
--zmiana hasła
IF @opcja=2
  BEGIN
	IF EXISTS (SELECT * FROM  dbo.Users WHERE [login]=@login AND haslo=@haslo AND id between 777000000 AND 778000000)
	  BEGIN
		SET NOCOUNT ON
	  	UPDATE dbo.Users SET haslo=@haslo_nowe, DataOstatniejZmianyHasla=GETDATE()
		WHERE [login]=@login AND id between 777000000 AND 778000000 
		SET NOCOUNT OFF			
		SELECT 1 AS wynik
	  END
	ELSE  
	  SELECT 0 AS wynik
  END
--nowy user
IF @opcja=3
  BEGIN
	SELECT @id_max=MAX(id) FROM dbo.Users WHERE id between 777000000 AND 778000000 
	INSERT INTO [Uzytkownicy].[dbo].[Users]
           ([ID],[login],[haslo],[Imie],[Nazwisko],[DataUtworzenia],[aktywny],[DataOstatniejZmiany],[windkorp])
     VALUES
           (@id_max+1,@login,'13aea3342d9cf01b7a0591ce0e1d425c',@imie,@nazwisko, GETDATE(),1,GETDATE(),@grupa)
	END

--zmiana danych
IF @opcja=4
  BEGIN
	INSERT INTO [Uzytkownicy].[dbo].[HistoriaUsers]
           ([id],[login],[Imie],[Nazwisko],[DataOstatniejZmianyHasla],[windkorp],[wpis_data],[wpis_osoba])
	SELECT [ID],[login],[Imie],[Nazwisko],[DataOstatniejZmianyHasla],[windkorp],GETDATE(),@uzytkownik  
	FROM [Uzytkownicy].[dbo].[Users] WHERE id=@id 

	UPDATE dbo.Users SET [login]=@login,Imie=@imie,Nazwisko=@nazwisko,windkorp=@grupa ,aktywny=@aktywny
	WHERE id=@id AND id between 777000000 AND 778000000 
  END

--reset hasła
IF @opcja=5
  BEGIN
	UPDATE dbo.Users SET haslo='13aea3342d9cf01b7a0591ce0e1d425c',DataOstatniejZmianyHasla=GETDATE()-33
	WHERE id=@id 

	INSERT INTO [Uzytkownicy].[dbo].[HistoriaUsers]
           ([id],[login],[Imie],[Nazwisko],[DataOstatniejZmianyHasla],[windkorp],[wpis_data],[wpis_osoba])
	SELECT [ID],[login],[Imie],[Nazwisko],[DataOstatniejZmianyHasla],[windkorp],GETDATE(),@uzytkownik  
	FROM [Uzytkownicy].[dbo].[Users] WHERE id=@id 

  END
	
END
GO
/****** Object:  StoredProcedure [dbo].[p_LogowanieWindKorp]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[p_LogowanieWindKorp]

@login VARCHAR(50)=null,
@haslo VARCHAR(50)=null,
@haslo_nowe VARCHAR(50)=NULL,
@imie VARCHAR(50)=NULL,
@nazwisko VARCHAR(50)=NULL,
@grupa VARCHAR(50)=NULL,
@uzytkownik VARCHAR(50)=NULL,
@aktywny BIT=1,
@id INT=NULL,
@opcja int

AS
BEGIN

DECLARE @id_max INT

--logowanie
IF @opcja=1
	SELECT windkorp,ISNULL(Imie,'') + ' ' + isnull(Nazwisko,'') AS imie_nazwisko,ISNULL(DataOstatniejZmianyHasla,'') AS DataOstatniejZmianyHasla,aktywny FROM dbo.Users 
	WHERE [login]=@login AND haslo=@haslo AND aktywny=1 AND id between 777000000 AND 778000000 
	
	
--zmiana hasła
IF @opcja=2
  BEGIN
	IF EXISTS (SELECT * FROM  dbo.Users WHERE [login]=@login AND haslo=@haslo AND id between 777000000 AND 778000000)
	  BEGIN
		SET NOCOUNT ON
	  	UPDATE dbo.Users SET haslo=@haslo_nowe, DataOstatniejZmianyHasla=GETDATE()
		WHERE [login]=@login AND id between 777000000 AND 778000000 
		SET NOCOUNT OFF			
		SELECT 1 AS wynik
	  END
	ELSE  
	  SELECT 0 AS wynik
  END
--nowy user
IF @opcja=3
  BEGIN
	SELECT @id_max=MAX(id) FROM dbo.Users WHERE id between 777000000 AND 778000000 
	INSERT INTO [Uzytkownicy].[dbo].[Users]
           ([ID],[login],[haslo],[Imie],[Nazwisko],[DataUtworzenia],[aktywny],[DataOstatniejZmiany],[windkorp])
     VALUES
           (@id_max+1,@login,'13aea3342d9cf01b7a0591ce0e1d425c',@imie,@nazwisko, GETDATE(),1,GETDATE(),@grupa)
	END

--zmiana danych
IF @opcja=4
  BEGIN
	INSERT INTO [Uzytkownicy].[dbo].[HistoriaUsers]
           ([id],[login],[Imie],[Nazwisko],[DataOstatniejZmianyHasla],[windkorp],[wpis_data],[wpis_osoba])
	SELECT [ID],[login],[Imie],[Nazwisko],[DataOstatniejZmianyHasla],[windkorp],GETDATE(),@uzytkownik  
	FROM [Uzytkownicy].[dbo].[Users] WHERE id=@id 

	UPDATE dbo.Users SET [login]=@login,Imie=@imie,Nazwisko=@nazwisko,windkorp=@grupa ,aktywny=@aktywny
	WHERE id=@id AND id between 777000000 AND 778000000 
  END

--reset hasła
IF @opcja=5
  BEGIN
	UPDATE dbo.Users SET haslo='13aea3342d9cf01b7a0591ce0e1d425c',DataOstatniejZmianyHasla=GETDATE()-33
	WHERE id=@id 

	INSERT INTO [Uzytkownicy].[dbo].[HistoriaUsers]
           ([id],[login],[Imie],[Nazwisko],[DataOstatniejZmianyHasla],[windkorp],[wpis_data],[wpis_osoba])
	SELECT [ID],[login],[Imie],[Nazwisko],[DataOstatniejZmianyHasla],[windkorp],GETDATE(),@uzytkownik  
	FROM [Uzytkownicy].[dbo].[Users] WHERE id=@id 

  END
	
END
GO
/****** Object:  View [dbo].[DomenyAplikacji]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[DomenyAplikacji]
AS
SELECT     IdApplicationDomain AS Id, IdApplication AS AplikacjaId, IdCompany AS FirmaId
FROM         dbo.ApplicationDomain
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ApplicationDomain"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 110
               Right = 224
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DomenyAplikacji'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'DomenyAplikacji'
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderGetDependetNoSKOKByBranch]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderGetDependetNoSKOKByBranch] @IdBranch INT

AS


SELECT c.IdCompany, c.Name, b.NoSKOK --IdUser, UserLevel 
FROM  dbo.Branch b 
INNER JOIN dbo.Company c ON b.NoSKOK = c.NoSKOK 
WHERE b.IdBranch = @IdBranch
GROUP BY c.IdCompany, c.Name, b.NoSKOK
GO
/****** Object:  StoredProcedure [dbo].[MvcUserManagement_CreateBranch]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MvcUserManagement_CreateBranch] 
    @Name VARCHAR(500),
    @NoBranch INT,
    @IdCompany INT,
    @IdRegion INT,
    @IdParent INT = NULL,
    @IsPK BIT = NULL,
    @NoSKOK INT
AS 

 DECLARE @IdBranch AS INT
 
 SET @IdBranch = (SELECT MAX(IdBranch)+1 FROM dbo.Branch WHERE IdCompany = @IdCompany)
 
 IF @IdBranch IS NULL
 BEGIN
	SET @IdBranch = ((SELECT Preffix FROM dbo.Company WHERE IdCompany = @IdCompany) * 1000000 )+ 1
 END
 
 INSERT INTO [dbo].Branch
        ( IdBranch ,
          Name ,
          NoBranch ,
          CreateDate ,
          IsActive ,
          IdCompany ,
          IdRegion,
          IdParent,
          IsPK,
          NoSKOK
        )
     VALUES
           (@IdBranch
           ,@Name
           ,@NoBranch
           ,GETDATE()
           ,1
           ,@IdCompany
           ,@IdRegion
           ,@IdParent
		   ,@IsPK
		   ,@NoSKOK
		   )
GO
/****** Object:  StoredProcedure [dbo].[HSOLoginProviderUpdateUserData]    Script Date: 02/27/2014 07:00:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[HSOLoginProviderUpdateUserData] @IdUser INT, @Footer VARCHAR(MAX)

AS

DECLARE @chk INT

SELECT COUNT(*) FROM UserData 
WHERE IdUser = @IdUser

IF @chk > 0
	UPDATE dbo.UserData SET Footer = @Footer WHERE IdUser = @IdUser
ELSE
	INSERT INTO dbo.UserData (IdUser, Footer) VALUES(@IdUser, @Footer)
GO
/****** Object:  StoredProcedure [dbo].[p_UserInfo]    Script Date: 02/27/2014 07:00:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[p_UserInfo] @IdUser INT

AS

SELECT 
	u.Imie, 
	u.Nazwisko, 
	ISNULL(s.Nazwa, '') AS Stanowisko,
	ISNULL(j.Nazwa, '') AS Dzial,
	ISNULL(u.Kierownik, '') AS IdKierownika,
	ISNULL(u1.Nazwisko, '') AS NazwiskoKierownika,
	ISNULL(u1.Imie, '') AS ImieKierownika, 
	ISNULL(u.stanowisko, 0) AS StanowiskoId,
	ISNULL(u.Firma, 0) AS FirmaId,
	ISNULL(f.Nazwa, '') AS Firma,
	ISNULL(f.Adres, '') AS FirmaAdres,
	ISNULL(f.Suffix, '') AS Suffix,
	ISNULL(u.Dzial, 0) AS DzialId
FROM Users u
	LEFT JOIN Stanowiska s ON u.Stanowisko = s.Id
	LEFT JOIN Jednostki j ON u.dzial = j.Id
	LEFT JOIN Users u1 ON u.Kierownik = u1.Id
	LEFT JOIN dbo.Firma f ON u.Firma = f.FirmaId
WHERE u.Id = @IdUser AND u.aktywny = 1
GO
/****** Object:  View [dbo].[v_StrukturaPodleglosciWspolnotaFinanse]    Script Date: 02/27/2014 07:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_StrukturaPodleglosciWspolnotaFinanse]
AS
SELECT     ID, Kierownik, SimpleID
FROM         dbo.Users
WHERE     (Firma = 16) AND (aktywny = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_StrukturaPodleglosciWspolnotaFinanse'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_StrukturaPodleglosciWspolnotaFinanse'
GO
/****** Object:  View [dbo].[v_StrukturaPodleglosciTZSPZOO]    Script Date: 02/27/2014 07:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_StrukturaPodleglosciTZSPZOO]
AS
SELECT     ID, Kierownik, SimpleID
FROM         dbo.Users
WHERE     (Firma = 17) AND (aktywny = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_StrukturaPodleglosciTZSPZOO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_StrukturaPodleglosciTZSPZOO'
GO
/****** Object:  View [dbo].[v_StrukturaPodleglosciStefczykFinanse]    Script Date: 02/27/2014 07:00:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_StrukturaPodleglosciStefczykFinanse]
AS
SELECT     ID, Kierownik, SimpleID
FROM         dbo.Users
WHERE     (Firma = 14) AND (aktywny = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Users"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 254
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_StrukturaPodleglosciStefczykFinanse'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_StrukturaPodleglosciStefczykFinanse'
GO
/****** Object:  Default [DF_User_IsTest]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_IsTest]  DEFAULT ((0)) FOR [IsTest]
GO
/****** Object:  ForeignKey [FK_ApplicationDomain_Application]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[ApplicationDomain]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationDomain_Application] FOREIGN KEY([IdApplication])
REFERENCES [dbo].[Application] ([IdApplication])
GO
ALTER TABLE [dbo].[ApplicationDomain] CHECK CONSTRAINT [FK_ApplicationDomain_Application]
GO
/****** Object:  ForeignKey [FK_ApplicationDomain_Company]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[ApplicationDomain]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationDomain_Company] FOREIGN KEY([IdCompany])
REFERENCES [dbo].[Company] ([IdCompany])
GO
ALTER TABLE [dbo].[ApplicationDomain] CHECK CONSTRAINT [FK_ApplicationDomain_Company]
GO
/****** Object:  ForeignKey [FK_Branch_Branch]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[Branch]  WITH CHECK ADD  CONSTRAINT [FK_Branch_Branch] FOREIGN KEY([IdParent])
REFERENCES [dbo].[Branch] ([IdBranch])
GO
ALTER TABLE [dbo].[Branch] CHECK CONSTRAINT [FK_Branch_Branch]
GO
/****** Object:  ForeignKey [FK_Branch_Region]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[Branch]  WITH CHECK ADD  CONSTRAINT [FK_Branch_Region] FOREIGN KEY([IdRegion])
REFERENCES [dbo].[Region] ([IdRegion])
GO
ALTER TABLE [dbo].[Branch] CHECK CONSTRAINT [FK_Branch_Region]
GO
/****** Object:  ForeignKey [FK_Department_Company]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[Department]  WITH CHECK ADD  CONSTRAINT [FK_Department_Company] FOREIGN KEY([IdCompany])
REFERENCES [dbo].[Company] ([IdCompany])
GO
ALTER TABLE [dbo].[Department] CHECK CONSTRAINT [FK_Department_Company]
GO
/****** Object:  ForeignKey [FK_Job_Company]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[Job]  WITH CHECK ADD  CONSTRAINT [FK_Job_Company] FOREIGN KEY([IdCompany])
REFERENCES [dbo].[Company] ([IdCompany])
GO
ALTER TABLE [dbo].[Job] CHECK CONSTRAINT [FK_Job_Company]
GO
/****** Object:  ForeignKey [FK_JobRole_Job]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[JobRole]  WITH CHECK ADD  CONSTRAINT [FK_JobRole_Job] FOREIGN KEY([IdJob])
REFERENCES [dbo].[Job] ([IdJob])
GO
ALTER TABLE [dbo].[JobRole] CHECK CONSTRAINT [FK_JobRole_Job]
GO
/****** Object:  ForeignKey [FK_Region_Company]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[Region]  WITH CHECK ADD  CONSTRAINT [FK_Region_Company] FOREIGN KEY([IdCompany])
REFERENCES [dbo].[Company] ([IdCompany])
GO
ALTER TABLE [dbo].[Region] CHECK CONSTRAINT [FK_Region_Company]
GO
/****** Object:  ForeignKey [FK_Role_Role]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[Role]  WITH CHECK ADD  CONSTRAINT [FK_Role_Role] FOREIGN KEY([IdApplication])
REFERENCES [dbo].[Application] ([IdApplication])
GO
ALTER TABLE [dbo].[Role] CHECK CONSTRAINT [FK_Role_Role]
GO
/****** Object:  ForeignKey [FK_User_Company]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Company] FOREIGN KEY([IdCompany])
REFERENCES [dbo].[Company] ([IdCompany])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Company]
GO
/****** Object:  ForeignKey [FK_User_Department]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Department] FOREIGN KEY([IdDepartment])
REFERENCES [dbo].[Department] ([IdDepartment])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Department]
GO
/****** Object:  ForeignKey [FK_User_Job]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_Job] FOREIGN KEY([IdJob])
REFERENCES [dbo].[Job] ([IdJob])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_Job]
GO
/****** Object:  ForeignKey [FK_User_User]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK_User_User] FOREIGN KEY([IdManager])
REFERENCES [dbo].[User] ([IdUser])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK_User_User]
GO
/****** Object:  ForeignKey [FK_UserData_User]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[UserData]  WITH CHECK ADD  CONSTRAINT [FK_UserData_User] FOREIGN KEY([IdUser])
REFERENCES [dbo].[User] ([IdUser])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[UserData] CHECK CONSTRAINT [FK_UserData_User]
GO
/****** Object:  ForeignKey [FK_UserRole_Role]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_Role] FOREIGN KEY([IdRole])
REFERENCES [dbo].[Role] ([IdRole])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_Role]
GO
/****** Object:  ForeignKey [FK_UserRole_User]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_User] FOREIGN KEY([IdUser])
REFERENCES [dbo].[User] ([IdUser])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_User]
GO
/****** Object:  ForeignKey [FK_UsersBranches_Branch]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[UsersBranches]  WITH CHECK ADD  CONSTRAINT [FK_UsersBranches_Branch] FOREIGN KEY([IdBranch])
REFERENCES [dbo].[Branch] ([IdBranch])
GO
ALTER TABLE [dbo].[UsersBranches] CHECK CONSTRAINT [FK_UsersBranches_Branch]
GO
/****** Object:  ForeignKey [FK_UsersBranches_Job]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[UsersBranches]  WITH CHECK ADD  CONSTRAINT [FK_UsersBranches_Job] FOREIGN KEY([IdJob])
REFERENCES [dbo].[Job] ([IdJob])
GO
ALTER TABLE [dbo].[UsersBranches] CHECK CONSTRAINT [FK_UsersBranches_Job]
GO
/****** Object:  ForeignKey [FK_UsersBranches_User]    Script Date: 02/27/2014 07:00:32 ******/
ALTER TABLE [dbo].[UsersBranches]  WITH CHECK ADD  CONSTRAINT [FK_UsersBranches_User] FOREIGN KEY([IdUser])
REFERENCES [dbo].[User] ([IdUser])
GO
ALTER TABLE [dbo].[UsersBranches] CHECK CONSTRAINT [FK_UsersBranches_User]
GO
